﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppImprimante;

namespace AppImprimantes
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Model model = new Model();

            if (model.TryMysqlConn())
            {
                model.GetBaseData();
                Views.MainView view = new Views.MainView();
                Views.Login login = new Views.Login();
                model.Frm = view;
                view.GetAllFrames();
                Application.Run(login);
            }
            else
                MessageBox.Show("Une erreur de connexion est survenue.\nMerci de vérifier votre connexion internet.\n\n" + model.e + "\n","Erreur de connexion");
        }
    }
}
