﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient.Authentication;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using static AppImprimantes.Variables;
using System.Collections;
using System.Drawing;
using System.ComponentModel;
using AppImprimante;
using System.Security.Cryptography;

namespace AppImprimantes
{
    public class Model
    {
        private MySqlConnection cnn;

        public static FileReader fileReader;  //Lecteur de fichiers
        public static string triFileNameImp = "commandesTrisImp.txt";  //Nom du fichier à lire
        public static string triFileNameCon = "commandesTrisCon.txt";  //Nom du fichier à lire
        public static string triFileNameAll = "commandesTrisAll.txt";  //Nom du fichier à lire

        private Views.MainView frm;
        public Views.MainView Frm { get { return frm; } set{ frm = value; } }

        public Exception e;

        private string USERNAME = "root";
        private string PASSWORD = "root";

        public Session session = new Session();

        private const string REGEX_NAME = @"^[a-zA-Z-'éàè]*$";
        private const string REGEX_EMAIL = @"^.{2,}@[a-zA-Z]{2,20}\..{2,}$";
        private const string REGEX_PASSWORD = @"^.{6,}$";

        public string RegexName
        {
            get { return REGEX_NAME; }
        }
        public string RegexEmail
        {
            get { return REGEX_EMAIL; }
        }
        public string RegexPassword
        {
            get { return REGEX_PASSWORD; }
        }
        public string DisplayHashCode(String Operand)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(Operand));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        public bool TryConnectUser(string email, string password)
        {
            string mail = null;
            string pass = null;
            int id1 = int.MaxValue;
            int id2 = 0;
            List<string> end = new List<string>();
            bool ok = false;
            string cmd = null;

            try
            {
                cmd = $"SELECT cliEmail FROM t_client WHERE cliEmail = '{email}'";
                mail = (string)SimpleDataQuerry<string>(cmd);
                cmd = $"SELECT cliPassword FROM t_client WHERE cliPassword = '{DisplayHashCode(password)}'";
                pass = (string)SimpleDataQuerry<string>(cmd);
                cmd = $"SELECT id_Client FROM t_client WHERE cliEmail = '{email}'";
                id1 = (int)SimpleDataQuerry<int>(cmd);
                cmd = $"SELECT id_Client FROM t_client WHERE cliPassword = '{DisplayHashCode(password)}'";
                id2 = (int)SimpleDataQuerry<int>(cmd);

                try
                {
                    if (id1 == id2)
                    {
                        ok = true;
                    }
                }
                catch
                {

                }
            }
            catch { }

            try
            {
                end.Add(mail);
            }
            catch
            {
            }
            try
            {
                end.Add(pass);
            }
            catch
            {
            }

            try
            {
                if (end.Count == 2 && ok)
                {


                    List<string> name = new List<string>();
                    List<string> surename = new List<string>();
                    List<string> id = new List<string>();
                    cmd = $"SELECT cliName FROM t_client WHERE cliPassword = '{DisplayHashCode(password)}'";
                    name = ((List<object>)SimpleDataQuerry<List<string>>(cmd)).OfType<string>().ToList();

                    cmd = $"SELECT cliSurname FROM t_client WHERE cliPassword = '{DisplayHashCode(password)}'";
                    surename = ((List<object>)SimpleDataQuerry<List<string>>(cmd)).OfType<string>().ToList();

                    /*cmd= $"SELECT id_Client FROM t_client WHERE cliPassword = '{DisplayHashCode(password)}'";
                    id = ((List<object>)SimpleDataQuerry<List<string>>(cmd)).OfType<string>().ToList();*/
                    session.Connection(name[0], surename[0], email , 1);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public void InsertUser(string name, string surname, string email, string password)
        {
            string cmd = $"INSERT INTO t_client (cliName, cliSurname, cliRegisterYears, cliEmail , cliPassword) VALUES ('{name}','{surname}','{DateTime.Now.Date.ToString("yyyy-MM-dd")}','{email}','{password}'); ";
            SimpleDataQuerry<bool>(cmd);
        }
        public void InsertCommandP(string idconsomable , string idclient)
        {
            string cmd = $"INSERT INTO t_order (orDate , id_consomable , id_client) VALUES ('{DateTime.Now.Date.ToString("yyyy-MM-dd")}','{idconsomable}','{idclient}'); ";
            SimpleDataQuerry<bool>(cmd);
        }
        public void InsertCommandC(string idprinter, string idclient)
        {
            string cmd = $"INSERT INTO t_order (id_printer , orDate , id_printer , id_client) VALUES ('{DateTime.Now.Date.ToString("yyyy-MM-dd")}','{idprinter}','{idclient}'); ";
            SimpleDataQuerry<bool>(cmd);
        }

        public void GetBaseData()
        {

            GiveViewRef();

            GetImpBaseData();
            GetImprimantesPrices();
            GetCartoucheBaseData();
            GetCartouchesPrices();

            GetTris();
        }

        public void GetArticle(string model)
        {
            List<object[]> article = new List<object[]>();

            string cmd = "SELECT `id_printer`, `priModele`,`priMark`,`priResolution`,`priHeight`,`priWidth`,`priLenght`,`priPrintSpeed`,`priReleaseDate`,`priWeight`, `priPaper`, `priType`  FROM `t_printer` WHERE `priModele` = \"" + model +"\"";
            articleInfo = new List<string>();

            TableDataQuerry(cmd, article, 1);

            if (article.Count != 0)
            {
                foreach (object o in article[0])
                    articleInfo.Add(o.ToString());

                cmd = "SELECT t_consomable.conModele FROM t_printer INNER JOIN t_consomable ON t_printer.id_Consomable = t_consomable.id_Consomable WHERE `priModele` = \"" + model + "\"";
                articleInfo.Add((string)SimpleDataQuerry<string>(cmd));
            }
        }

        private void GetTris()
        {
            fileReader = new FileReader(triFileNameImp);
            CommandesTrisImp = fileReader.getFile();

            fileReader = new FileReader(triFileNameCon);
            CommandesTrisCon = fileReader.getFile();

            fileReader = new FileReader(triFileNameAll);
            CommandesTrisAll = fileReader.getFile();
        }

        private void GiveViewRef()
        {
            Views.MainView.LocalModel = this;
        }

        public bool TryMysqlConn()
        {
            string connetionString;
            connetionString = "server=localhost;database=p042database;uid=" + USERNAME + ";pwd=" + PASSWORD + ";";
            cnn = new MySqlConnection(connetionString);
            try
            {
                cnn.Open();
                return true;
            }
            catch (Exception error)
            {
                e = error;
                return false;
            }
        }

        private void GetImpBaseData()
        {
            string cmd = "SELECT COUNT(DISTINCT `priMark`) FROM `t_printer`;";
            btnImpCount = Convert.ToInt32(SimpleDataQuerry<int>(cmd));

            cmd = "SELECT DISTINCT `priMark` FROM `t_printer`";
            marquesImprimantes = ((List<object>)SimpleDataQuerry<List<object>>(cmd)).OfType<string>().ToList();
            marquesImprimantes.Insert(0,"Toutes");
        }

        public void GetCartouchesPrices()
        {
            string cmd = "SELECT MAX(`conPrice`) FROM `t_consomable`;";
            priConMax = Convert.ToInt64(SimpleDataQuerry<float>(cmd));

            cmd = "SELECT MIN(`conPrice`) FROM `t_consomable`;";
            priConMin = Convert.ToInt64(SimpleDataQuerry<float>(cmd));
        }

        private void GetCartoucheBaseData()
        {
            string cmd = "SELECT COUNT(DISTINCT `conMark`) FROM `t_consomable`;";
            btnCartoucheCount = Convert.ToInt32(SimpleDataQuerry<int>(cmd));

            cmd = "SELECT DISTINCT `conMark` FROM `t_consomable`;";
            marquesConsomables = ((List<object>)SimpleDataQuerry<List<object>>(cmd)).OfType<string>().ToList();
            marquesConsomables.Insert(0, "Toutes");
        }

        public void GetImprimantesPrices()
        {
            string cmd = "SELECT MAX(t_price.evPrice) FROM t_price WHERE t_price.evYears = (SELECT MAX(t_price.evYears) FROM t_price) GROUP BY t_price.evYears;";
            priImpMax = Convert.ToInt64(SimpleDataQuerry<float>(cmd));

            cmd = "SELECT MIN(t_price.evPrice) FROM t_price WHERE t_price.evYears = (SELECT MAX(t_price.evYears) FROM t_price) GROUP BY t_price.evYears;;";
            priImpMin = Convert.ToInt64(SimpleDataQuerry<float>(cmd));
        }

        public void GetImpByMarque(string marque, string tri, int prixMin, int prixMax)
        {
            int lenght = 2;
            string cmd;
            List<object[]> table = new List<object[]>();

            if (marque != "Toutes")
                cmd = $"SELECT t_printer.priModele , t_printer.priImage , t_price.evPrice, t_printer.priWidth, t_printer.priHeight, t_printer.priLenght FROM t_printer INNER JOIN t_price ON t_printer.id_printer = t_price.id_printer WHERE t_price.evYears = (SELECT MAX(t_price.evYears) FROM t_printer) and t_price.evPrice >= " + prixMin + " and t_price.evPrice <= " + prixMax+" and t_printer.priMark = \"" + marque + "\" group by t_printer.id_printer " + tri + ";";
            else
                cmd = "SELECT t_printer.priModele , t_printer.priImage , t_price.evPrice , t_printer.priWidth, t_printer.priHeight, t_printer.priLenght FROM t_printer INNER JOIN t_price ON t_printer.id_printer = t_price.id_printer WHERE t_price.evYears = (SELECT MAX(t_price.evYears) FROM t_printer) and t_price.evPrice >= " + prixMin + " and t_price.evPrice <= " + prixMax + " group by t_printer.id_printer " + tri  + ";";

            TableDataQuerry(cmd, table, lenght);

            TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));

            for (int i = 0; i < table.Count; i++)
                frameList.Add(new Frame((Bitmap)tc.ConvertFrom(table[i][1]), table[i][0].ToString(), Convert.ToDouble(table[i][2]), this, (int)table[i][5], (int)table[i][3], (int)table[i][4]));
        }

        public void GetConByMarque(string marque, string tri, int prixMin, int prixMax)
        {
            int lenght = 2;
            string cmd;
            List<object[]> table = new List<object[]>();

            if (marque != "Toutes")
                cmd = "SELECT DISTINCT conModele,conPrice FROM `t_consomable` WHERE conPrice >= " + prixMin + " and conPrice <= " + prixMax + " AND conMark = \"" + marque + "\"  " + tri + ";";
            else
                cmd = "SELECT DISTINCT conModele,conPrice FROM `t_consomable` WHERE conPrice >= " + prixMin + " and conPrice <= " + prixMax + " " + tri + ";";

            TableDataQuerry(cmd, table, lenght);

            for (int i = 0; i < table.Count; i++)
                frameList.Add(new Frame(guiAppImprimantes_menetreyar.Resources.Images.cartouche_generique_h337_noire,table[i][0].ToString(), Convert.ToDouble(table[i][1]),this,0,0,0));
        }

        /// <summary>
        /// Renvoie les valeurs d'une commande sql sous forme d'objet.
        /// Ne peut lire que des structures unidimensionels ou des valeurs uniques.
        /// </summary>
        /// <param name="command">Commande</param>
        /// <param name="value">Type de la structure unidimensionelle</param>
        /// <returns>Structure unidimensionelle</returns>
        public object SimpleDataQuerry<T>(string command)
        {
            TryMysqlConn();

            object value = null;

            List<object> listObjects = new List<object>();

            bool isAList = typeof(T).IsGenericType;

            using (var reader = new MySqlCommand(command, cnn).ExecuteReader())
            {
                while (reader.Read())
                {
                    if (isAList)
                    {
                        Type test = typeof(T);
                        listObjects.Add(Convert.ChangeType(reader.GetValue(0), typeof(object)));
                    }
                    else
                    {
                        value = (Convert.ChangeType(reader.GetValue(0), typeof(object)));
                    }
                }
            }

            if (isAList)
                value = listObjects;

            cnn.Close();

            return value;
        }


        /// <summary>
        /// Renvoie les valeurs d'une commande sql sous forme d'objet.
        /// Ne peut lire que des structures unidimensionelles.
        /// </summary>
        /// <param name="command">Commande</param>
        /// <param name="values">Type de la structure bidimensionelle</param>
        /// <returns>Structure bidimensionelle</returns>
        public void TableDataQuerry(string command, List<object[]> values, int lenght)
        {
            TryMysqlConn();
            using (var reader = new MySqlCommand(command, cnn).ExecuteReader())
            {
                int counter = 0;

                while (reader.Read())
                {
                    object[] o = new object[reader.FieldCount];
                    reader.GetValues(o);
                    values.Add(o);
                    counter++;
                }
            }

            cnn.Close();
        }
    }
}
