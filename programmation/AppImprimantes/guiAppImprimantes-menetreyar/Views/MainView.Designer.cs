﻿namespace AppImprimantes.Views
{
    partial class MainView
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMaximize = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlArticle = new System.Windows.Forms.Panel();
            this.lblAPrix = new System.Windows.Forms.Label();
            this.btnBuy = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblFormat = new System.Windows.Forms.RichTextBox();
            this.lblAHeight = new System.Windows.Forms.Label();
            this.lblAWidth = new System.Windows.Forms.Label();
            this.lblALenght = new System.Windows.Forms.Label();
            this.lblASpeed = new System.Windows.Forms.Label();
            this.lblARelease = new System.Windows.Forms.Label();
            this.lblAWeight = new System.Windows.Forms.Label();
            this.lblARes = new System.Windows.Forms.Label();
            this.lblAType = new System.Windows.Forms.Label();
            this.lblAMarque = new System.Windows.Forms.Label();
            this.pcbArticle = new System.Windows.Forms.PictureBox();
            this.lblAName = new System.Windows.Forms.Label();
            this.pnlSchearch = new System.Windows.Forms.Panel();
            this.cbbTrier = new System.Windows.Forms.ComboBox();
            this.lblTrier = new System.Windows.Forms.Label();
            this.lblArticle = new System.Windows.Forms.Label();
            this.cbbArticle = new System.Windows.Forms.ComboBox();
            this.lblMarque = new System.Windows.Forms.Label();
            this.cbbMarque = new System.Windows.Forms.ComboBox();
            this.lblPrixMax = new System.Windows.Forms.Label();
            this.lblPrixMin = new System.Windows.Forms.Label();
            this.trackBarMin = new System.Windows.Forms.TrackBar();
            this.trackBarMax = new System.Windows.Forms.TrackBar();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.flpFrames = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlImprimanteInfo = new System.Windows.Forms.Panel();
            this.pnlImpDrawBar = new System.Windows.Forms.Panel();
            this.pnlNavigationBar = new System.Windows.Forms.Panel();
            this.btnAccount = new System.Windows.Forms.Button();
            this.btnConsommables = new System.Windows.Forms.Button();
            this.btnImprimantes = new System.Windows.Forms.Button();
            this.pnlConsommablesInfo = new System.Windows.Forms.Panel();
            this.pnlCartoucheDrawBar = new System.Windows.Forms.Panel();
            this.btnHideArticle = new System.Windows.Forms.Button();
            this.lblACon = new System.Windows.Forms.RichTextBox();
            this.pnlMenu.SuspendLayout();
            this.pnlArticle.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbArticle)).BeginInit();
            this.pnlSchearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMax)).BeginInit();
            this.pnlContent.SuspendLayout();
            this.pnlImprimanteInfo.SuspendLayout();
            this.pnlNavigationBar.SuspendLayout();
            this.pnlConsommablesInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnClose.Location = new System.Drawing.Point(720, 4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(25, 25);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnMaximize
            // 
            this.btnMaximize.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMaximize.Font = new System.Drawing.Font("Microsoft JhengHei UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMaximize.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnMaximize.Location = new System.Drawing.Point(692, 4);
            this.btnMaximize.Name = "btnMaximize";
            this.btnMaximize.Size = new System.Drawing.Size(25, 25);
            this.btnMaximize.TabIndex = 1;
            this.btnMaximize.Text = "[]";
            this.btnMaximize.UseVisualStyleBackColor = true;
            this.btnMaximize.Click += new System.EventHandler(this.BtnMaximize_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.BackColor = System.Drawing.Color.Transparent;
            this.pnlMenu.Controls.Add(this.pnlArticle);
            this.pnlMenu.Controls.Add(this.pnlSchearch);
            this.pnlMenu.Controls.Add(this.pnlContent);
            this.pnlMenu.Location = new System.Drawing.Point(100, 32);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(645, 395);
            this.pnlMenu.TabIndex = 3;
            this.pnlMenu.Click += new System.EventHandler(this.HideArticle);
            this.pnlMenu.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // pnlArticle
            // 
            this.pnlArticle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlArticle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlArticle.Controls.Add(this.btnHideArticle);
            this.pnlArticle.Controls.Add(this.lblAPrix);
            this.pnlArticle.Controls.Add(this.btnBuy);
            this.pnlArticle.Controls.Add(this.panel1);
            this.pnlArticle.Controls.Add(this.lblARes);
            this.pnlArticle.Controls.Add(this.lblAType);
            this.pnlArticle.Controls.Add(this.lblAMarque);
            this.pnlArticle.Controls.Add(this.pcbArticle);
            this.pnlArticle.Controls.Add(this.lblAName);
            this.pnlArticle.Location = new System.Drawing.Point(6, 21);
            this.pnlArticle.Name = "pnlArticle";
            this.pnlArticle.Size = new System.Drawing.Size(615, 400);
            this.pnlArticle.TabIndex = 0;
            this.pnlArticle.Visible = false;
            // 
            // lblAPrix
            // 
            this.lblAPrix.AutoSize = true;
            this.lblAPrix.Font = new System.Drawing.Font("Open Sans", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblAPrix.ForeColor = System.Drawing.Color.LightSeaGreen;
            this.lblAPrix.Location = new System.Drawing.Point(257, 205);
            this.lblAPrix.Name = "lblAPrix";
            this.lblAPrix.Size = new System.Drawing.Size(54, 23);
            this.lblAPrix.TabIndex = 16;
            this.lblAPrix.Text = "Prix : ";
            // 
            // btnBuy
            // 
            this.btnBuy.BackColor = System.Drawing.Color.DarkCyan;
            this.btnBuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuy.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnBuy.Location = new System.Drawing.Point(475, 188);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(123, 40);
            this.btnBuy.TabIndex = 15;
            this.btnBuy.Text = "Ajouter au panier";
            this.btnBuy.UseVisualStyleBackColor = false;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lblACon);
            this.panel1.Controls.Add(this.lblFormat);
            this.panel1.Controls.Add(this.lblAHeight);
            this.panel1.Controls.Add(this.lblAWidth);
            this.panel1.Controls.Add(this.lblALenght);
            this.panel1.Controls.Add(this.lblASpeed);
            this.panel1.Controls.Add(this.lblARelease);
            this.panel1.Controls.Add(this.lblAWeight);
            this.panel1.Location = new System.Drawing.Point(19, 244);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(579, 133);
            this.panel1.TabIndex = 14;
            // 
            // lblFormat
            // 
            this.lblFormat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblFormat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblFormat.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblFormat.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblFormat.Location = new System.Drawing.Point(453, 77);
            this.lblFormat.Name = "lblFormat";
            this.lblFormat.Size = new System.Drawing.Size(100, 67);
            this.lblFormat.TabIndex = 13;
            this.lblFormat.Text = "Formats :           ";
            // 
            // lblAHeight
            // 
            this.lblAHeight.AutoSize = true;
            this.lblAHeight.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAHeight.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAHeight.Location = new System.Drawing.Point(21, 9);
            this.lblAHeight.Name = "lblAHeight";
            this.lblAHeight.Size = new System.Drawing.Size(68, 18);
            this.lblAHeight.TabIndex = 5;
            this.lblAHeight.Text = "Hauteur : ";
            // 
            // lblAWidth
            // 
            this.lblAWidth.AutoSize = true;
            this.lblAWidth.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAWidth.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAWidth.Location = new System.Drawing.Point(21, 58);
            this.lblAWidth.Name = "lblAWidth";
            this.lblAWidth.Size = new System.Drawing.Size(65, 18);
            this.lblAWidth.TabIndex = 6;
            this.lblAWidth.Text = "Largeur : ";
            // 
            // lblALenght
            // 
            this.lblALenght.AutoSize = true;
            this.lblALenght.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblALenght.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblALenght.Location = new System.Drawing.Point(21, 107);
            this.lblALenght.Name = "lblALenght";
            this.lblALenght.Size = new System.Drawing.Size(76, 18);
            this.lblALenght.TabIndex = 7;
            this.lblALenght.Text = "Longueur : ";
            // 
            // lblASpeed
            // 
            this.lblASpeed.AutoSize = true;
            this.lblASpeed.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblASpeed.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblASpeed.Location = new System.Drawing.Point(225, 9);
            this.lblASpeed.Name = "lblASpeed";
            this.lblASpeed.Size = new System.Drawing.Size(135, 18);
            this.lblASpeed.TabIndex = 8;
            this.lblASpeed.Text = "Vitesse par minutes :";
            // 
            // lblARelease
            // 
            this.lblARelease.AutoSize = true;
            this.lblARelease.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblARelease.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblARelease.Location = new System.Drawing.Point(225, 58);
            this.lblARelease.Name = "lblARelease";
            this.lblARelease.Size = new System.Drawing.Size(100, 18);
            this.lblARelease.TabIndex = 10;
            this.lblARelease.Text = "Date de sortie :";
            // 
            // lblAWeight
            // 
            this.lblAWeight.AutoSize = true;
            this.lblAWeight.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAWeight.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAWeight.Location = new System.Drawing.Point(225, 107);
            this.lblAWeight.Name = "lblAWeight";
            this.lblAWeight.Size = new System.Drawing.Size(49, 18);
            this.lblAWeight.TabIndex = 9;
            this.lblAWeight.Text = "Poids :";
            // 
            // lblARes
            // 
            this.lblARes.AutoSize = true;
            this.lblARes.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblARes.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblARes.Location = new System.Drawing.Point(258, 163);
            this.lblARes.Name = "lblARes";
            this.lblARes.Size = new System.Drawing.Size(84, 18);
            this.lblARes.TabIndex = 4;
            this.lblARes.Text = "Resolution : ";
            // 
            // lblAType
            // 
            this.lblAType.AutoSize = true;
            this.lblAType.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAType.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAType.Location = new System.Drawing.Point(258, 122);
            this.lblAType.Name = "lblAType";
            this.lblAType.Size = new System.Drawing.Size(47, 18);
            this.lblAType.TabIndex = 3;
            this.lblAType.Text = "Type : ";
            // 
            // lblAMarque
            // 
            this.lblAMarque.AutoSize = true;
            this.lblAMarque.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAMarque.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAMarque.Location = new System.Drawing.Point(258, 80);
            this.lblAMarque.Name = "lblAMarque";
            this.lblAMarque.Size = new System.Drawing.Size(65, 18);
            this.lblAMarque.TabIndex = 2;
            this.lblAMarque.Text = "Marque : ";
            // 
            // pcbArticle
            // 
            this.pcbArticle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(84)))), ((int)(((byte)(84)))));
            this.pcbArticle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcbArticle.Location = new System.Drawing.Point(19, 22);
            this.pcbArticle.Name = "pcbArticle";
            this.pcbArticle.Size = new System.Drawing.Size(220, 200);
            this.pcbArticle.TabIndex = 1;
            this.pcbArticle.TabStop = false;
            // 
            // lblAName
            // 
            this.lblAName.AutoSize = true;
            this.lblAName.Font = new System.Drawing.Font("Open Sans", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblAName.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblAName.Location = new System.Drawing.Point(279, 34);
            this.lblAName.Name = "lblAName";
            this.lblAName.Size = new System.Drawing.Size(135, 23);
            this.lblAName.TabIndex = 0;
            this.lblAName.Text = "Nom de l\'article";
            // 
            // pnlSchearch
            // 
            this.pnlSchearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.pnlSchearch.Controls.Add(this.cbbTrier);
            this.pnlSchearch.Controls.Add(this.lblTrier);
            this.pnlSchearch.Controls.Add(this.lblArticle);
            this.pnlSchearch.Controls.Add(this.cbbArticle);
            this.pnlSchearch.Controls.Add(this.lblMarque);
            this.pnlSchearch.Controls.Add(this.cbbMarque);
            this.pnlSchearch.Controls.Add(this.lblPrixMax);
            this.pnlSchearch.Controls.Add(this.lblPrixMin);
            this.pnlSchearch.Controls.Add(this.trackBarMin);
            this.pnlSchearch.Controls.Add(this.trackBarMax);
            this.pnlSchearch.Location = new System.Drawing.Point(15, 0);
            this.pnlSchearch.Name = "pnlSchearch";
            this.pnlSchearch.Size = new System.Drawing.Size(614, 100);
            this.pnlSchearch.TabIndex = 0;
            this.pnlSchearch.Click += new System.EventHandler(this.HideArticle);
            this.pnlSchearch.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // cbbTrier
            // 
            this.cbbTrier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbTrier.FormattingEnabled = true;
            this.cbbTrier.Location = new System.Drawing.Point(468, 34);
            this.cbbTrier.Name = "cbbTrier";
            this.cbbTrier.Size = new System.Drawing.Size(121, 21);
            this.cbbTrier.TabIndex = 11;
            this.cbbTrier.SelectedIndexChanged += new System.EventHandler(this.TestArticleAndSend);
            // 
            // lblTrier
            // 
            this.lblTrier.AutoSize = true;
            this.lblTrier.ForeColor = System.Drawing.SystemColors.Control;
            this.lblTrier.Location = new System.Drawing.Point(465, 13);
            this.lblTrier.Name = "lblTrier";
            this.lblTrier.Size = new System.Drawing.Size(53, 13);
            this.lblTrier.TabIndex = 10;
            this.lblTrier.Text = "Trier Par :";
            // 
            // lblArticle
            // 
            this.lblArticle.AutoSize = true;
            this.lblArticle.ForeColor = System.Drawing.SystemColors.Control;
            this.lblArticle.Location = new System.Drawing.Point(16, 67);
            this.lblArticle.Name = "lblArticle";
            this.lblArticle.Size = new System.Drawing.Size(39, 13);
            this.lblArticle.TabIndex = 9;
            this.lblArticle.Text = "Article:";
            this.lblArticle.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // cbbArticle
            // 
            this.cbbArticle.FormattingEnabled = true;
            this.cbbArticle.Location = new System.Drawing.Point(71, 62);
            this.cbbArticle.Name = "cbbArticle";
            this.cbbArticle.Size = new System.Drawing.Size(121, 21);
            this.cbbArticle.TabIndex = 8;
            this.cbbArticle.SelectedIndexChanged += new System.EventHandler(this.CbbArticle_SelectedIndexChanged);
            this.cbbArticle.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // lblMarque
            // 
            this.lblMarque.AutoSize = true;
            this.lblMarque.ForeColor = System.Drawing.SystemColors.Control;
            this.lblMarque.Location = new System.Drawing.Point(16, 18);
            this.lblMarque.Name = "lblMarque";
            this.lblMarque.Size = new System.Drawing.Size(49, 13);
            this.lblMarque.TabIndex = 7;
            this.lblMarque.Text = "Marque :";
            this.lblMarque.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // cbbMarque
            // 
            this.cbbMarque.FormattingEnabled = true;
            this.cbbMarque.Location = new System.Drawing.Point(71, 15);
            this.cbbMarque.Name = "cbbMarque";
            this.cbbMarque.Size = new System.Drawing.Size(121, 21);
            this.cbbMarque.TabIndex = 6;
            this.cbbMarque.SelectedIndexChanged += new System.EventHandler(this.TestArticleAndSend);
            this.cbbMarque.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // lblPrixMax
            // 
            this.lblPrixMax.AutoSize = true;
            this.lblPrixMax.ForeColor = System.Drawing.SystemColors.Control;
            this.lblPrixMax.Location = new System.Drawing.Point(315, 62);
            this.lblPrixMax.Name = "lblPrixMax";
            this.lblPrixMax.Size = new System.Drawing.Size(76, 13);
            this.lblPrixMax.TabIndex = 5;
            this.lblPrixMax.Text = "Prix maximum :";
            this.lblPrixMax.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // lblPrixMin
            // 
            this.lblPrixMin.AutoSize = true;
            this.lblPrixMin.ForeColor = System.Drawing.SystemColors.Control;
            this.lblPrixMin.Location = new System.Drawing.Point(315, 13);
            this.lblPrixMin.Name = "lblPrixMin";
            this.lblPrixMin.Size = new System.Drawing.Size(73, 13);
            this.lblPrixMin.TabIndex = 4;
            this.lblPrixMin.Text = "Prix minimum :";
            this.lblPrixMin.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // trackBarMin
            // 
            this.trackBarMin.Location = new System.Drawing.Point(207, 10);
            this.trackBarMin.Name = "trackBarMin";
            this.trackBarMin.Size = new System.Drawing.Size(104, 45);
            this.trackBarMin.SmallChange = 100;
            this.trackBarMin.TabIndex = 1;
            this.trackBarMin.Scroll += new System.EventHandler(this.TrackBarMin_ValueChanged);
            this.trackBarMin.ValueChanged += new System.EventHandler(this.TrackBarMin_ValueChanged);
            this.trackBarMin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackMinBar_ValueChanged);
            // 
            // trackBarMax
            // 
            this.trackBarMax.Location = new System.Drawing.Point(207, 60);
            this.trackBarMax.Name = "trackBarMax";
            this.trackBarMax.Size = new System.Drawing.Size(104, 45);
            this.trackBarMax.SmallChange = 100;
            this.trackBarMax.TabIndex = 0;
            this.trackBarMax.Scroll += new System.EventHandler(this.TrackBarMax_ValueChanged);
            this.trackBarMax.ValueChanged += new System.EventHandler(this.TrackBarMax_ValueChanged);
            this.trackBarMax.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrackMinBar_ValueChanged);
            // 
            // pnlContent
            // 
            this.pnlContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pnlContent.Controls.Add(this.flpFrames);
            this.pnlContent.Location = new System.Drawing.Point(15, 92);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(613, 303);
            this.pnlContent.TabIndex = 0;
            this.pnlContent.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // flpFrames
            // 
            this.flpFrames.BackColor = System.Drawing.Color.Transparent;
            this.flpFrames.Location = new System.Drawing.Point(0, 3);
            this.flpFrames.Name = "flpFrames";
            this.flpFrames.Padding = new System.Windows.Forms.Padding(10, 10, 40, 10);
            this.flpFrames.Size = new System.Drawing.Size(614, 300);
            this.flpFrames.TabIndex = 0;
            this.flpFrames.Click += new System.EventHandler(this.HideArticle);
            this.flpFrames.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // pnlImprimanteInfo
            // 
            this.pnlImprimanteInfo.BackColor = System.Drawing.Color.LightSeaGreen;
            this.pnlImprimanteInfo.Controls.Add(this.pnlImpDrawBar);
            this.pnlImprimanteInfo.Location = new System.Drawing.Point(97, 50);
            this.pnlImprimanteInfo.Name = "pnlImprimanteInfo";
            this.pnlImprimanteInfo.Size = new System.Drawing.Size(200, 200);
            this.pnlImprimanteInfo.TabIndex = 0;
            this.pnlImprimanteInfo.Visible = false;
            this.pnlImprimanteInfo.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // pnlImpDrawBar
            // 
            this.pnlImpDrawBar.BackColor = System.Drawing.Color.Teal;
            this.pnlImpDrawBar.Location = new System.Drawing.Point(15, 0);
            this.pnlImpDrawBar.Name = "pnlImpDrawBar";
            this.pnlImpDrawBar.Size = new System.Drawing.Size(170, 200);
            this.pnlImpDrawBar.TabIndex = 0;
            // 
            // pnlNavigationBar
            // 
            this.pnlNavigationBar.BackColor = System.Drawing.Color.DarkCyan;
            this.pnlNavigationBar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlNavigationBar.Controls.Add(this.btnAccount);
            this.pnlNavigationBar.Controls.Add(this.btnConsommables);
            this.pnlNavigationBar.Controls.Add(this.btnImprimantes);
            this.pnlNavigationBar.Location = new System.Drawing.Point(0, 32);
            this.pnlNavigationBar.Name = "pnlNavigationBar";
            this.pnlNavigationBar.Size = new System.Drawing.Size(100, 419);
            this.pnlNavigationBar.TabIndex = 2;
            this.pnlNavigationBar.Click += new System.EventHandler(this.HideArticle);
            this.pnlNavigationBar.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // btnAccount
            // 
            this.btnAccount.FlatAppearance.BorderSize = 0;
            this.btnAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccount.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAccount.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnAccount.Location = new System.Drawing.Point(0, 137);
            this.btnAccount.Name = "btnAccount";
            this.btnAccount.Size = new System.Drawing.Size(100, 64);
            this.btnAccount.TabIndex = 7;
            this.btnAccount.Text = "Panier (0)";
            this.btnAccount.UseVisualStyleBackColor = true;
            this.btnAccount.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // btnConsommables
            // 
            this.btnConsommables.FlatAppearance.BorderSize = 0;
            this.btnConsommables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsommables.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsommables.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnConsommables.Location = new System.Drawing.Point(0, 70);
            this.btnConsommables.Name = "btnConsommables";
            this.btnConsommables.Size = new System.Drawing.Size(100, 64);
            this.btnConsommables.TabIndex = 6;
            this.btnConsommables.Text = "Consommables";
            this.btnConsommables.UseVisualStyleBackColor = true;
            this.btnConsommables.Click += new System.EventHandler(this.BtnConsommables_Click);
            this.btnConsommables.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // btnImprimantes
            // 
            this.btnImprimantes.FlatAppearance.BorderSize = 0;
            this.btnImprimantes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimantes.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimantes.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnImprimantes.Location = new System.Drawing.Point(0, 0);
            this.btnImprimantes.Name = "btnImprimantes";
            this.btnImprimantes.Size = new System.Drawing.Size(100, 64);
            this.btnImprimantes.TabIndex = 0;
            this.btnImprimantes.Text = "Imprimantes";
            this.btnImprimantes.UseVisualStyleBackColor = true;
            this.btnImprimantes.Click += new System.EventHandler(this.BtnImprimantes_Click);
            this.btnImprimantes.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // pnlConsommablesInfo
            // 
            this.pnlConsommablesInfo.BackColor = System.Drawing.Color.LightSeaGreen;
            this.pnlConsommablesInfo.Controls.Add(this.pnlCartoucheDrawBar);
            this.pnlConsommablesInfo.Location = new System.Drawing.Point(97, 121);
            this.pnlConsommablesInfo.Name = "pnlConsommablesInfo";
            this.pnlConsommablesInfo.Size = new System.Drawing.Size(200, 200);
            this.pnlConsommablesInfo.TabIndex = 1;
            this.pnlConsommablesInfo.Visible = false;
            this.pnlConsommablesInfo.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            // 
            // pnlCartoucheDrawBar
            // 
            this.pnlCartoucheDrawBar.BackColor = System.Drawing.Color.Teal;
            this.pnlCartoucheDrawBar.Location = new System.Drawing.Point(15, 0);
            this.pnlCartoucheDrawBar.Name = "pnlCartoucheDrawBar";
            this.pnlCartoucheDrawBar.Size = new System.Drawing.Size(170, 200);
            this.pnlCartoucheDrawBar.TabIndex = 0;
            // 
            // btnHideArticle
            // 
            this.btnHideArticle.FlatAppearance.BorderSize = 0;
            this.btnHideArticle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHideArticle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHideArticle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btnHideArticle.Location = new System.Drawing.Point(585, 3);
            this.btnHideArticle.Name = "btnHideArticle";
            this.btnHideArticle.Size = new System.Drawing.Size(25, 25);
            this.btnHideArticle.TabIndex = 4;
            this.btnHideArticle.Text = "X";
            this.btnHideArticle.UseVisualStyleBackColor = true;
            this.btnHideArticle.Click += new System.EventHandler(this.HideArticle);
            // 
            // lblACon
            // 
            this.lblACon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblACon.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblACon.Font = new System.Drawing.Font("Open Sans", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblACon.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblACon.Location = new System.Drawing.Point(453, 9);
            this.lblACon.Name = "lblACon";
            this.lblACon.Size = new System.Drawing.Size(100, 67);
            this.lblACon.TabIndex = 14;
            this.lblACon.Text = "Formats :           ";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.ClientSize = new System.Drawing.Size(750, 450);
            this.Controls.Add(this.pnlMenu);
            this.Controls.Add(this.pnlNavigationBar);
            this.Controls.Add(this.btnMaximize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pnlImprimanteInfo);
            this.Controls.Add(this.pnlConsommablesInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainView";
            this.Click += new System.EventHandler(this.HideArticle);
            this.MouseEnter += new System.EventHandler(this.BtnSideBar_MouseHover);
            this.pnlMenu.ResumeLayout(false);
            this.pnlArticle.ResumeLayout(false);
            this.pnlArticle.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbArticle)).EndInit();
            this.pnlSchearch.ResumeLayout(false);
            this.pnlSchearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarMax)).EndInit();
            this.pnlContent.ResumeLayout(false);
            this.pnlImprimanteInfo.ResumeLayout(false);
            this.pnlNavigationBar.ResumeLayout(false);
            this.pnlConsommablesInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMaximize;
        private System.Windows.Forms.Panel pnlNavigationBar;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlImprimanteInfo;
        private System.Windows.Forms.Button btnImprimantes;
        private System.Windows.Forms.Panel pnlImpDrawBar;
        private System.Windows.Forms.Button btnConsommables;
        private System.Windows.Forms.Panel pnlConsommablesInfo;
        private System.Windows.Forms.Panel pnlCartoucheDrawBar;
        private System.Windows.Forms.Panel pnlSchearch;
        private System.Windows.Forms.TrackBar trackBarMin;
        private System.Windows.Forms.TrackBar trackBarMax;
        private System.Windows.Forms.Label lblPrixMax;
        private System.Windows.Forms.Label lblPrixMin;
        private System.Windows.Forms.ComboBox cbbMarque;
        private System.Windows.Forms.Label lblMarque;
        private System.Windows.Forms.Label lblArticle;
        private System.Windows.Forms.ComboBox cbbArticle;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.FlowLayoutPanel flpFrames;
        private System.Windows.Forms.ComboBox cbbTrier;
        private System.Windows.Forms.Label lblTrier;
        private System.Windows.Forms.Panel pnlArticle;
        private System.Windows.Forms.PictureBox pcbArticle;
        private System.Windows.Forms.Label lblAName;
        private System.Windows.Forms.Label lblAMarque;
        private System.Windows.Forms.Label lblARes;
        private System.Windows.Forms.Label lblAType;
        private System.Windows.Forms.Label lblALenght;
        private System.Windows.Forms.Label lblAWidth;
        private System.Windows.Forms.Label lblAHeight;
        private System.Windows.Forms.Label lblARelease;
        private System.Windows.Forms.Label lblAWeight;
        private System.Windows.Forms.Label lblASpeed;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.RichTextBox lblFormat;
        private System.Windows.Forms.Label lblAPrix;
        private System.Windows.Forms.Button btnAccount;
        private System.Windows.Forms.Button btnHideArticle;
        private System.Windows.Forms.RichTextBox lblACon;
    }
}

