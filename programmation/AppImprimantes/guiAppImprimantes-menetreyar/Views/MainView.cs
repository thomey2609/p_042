﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AppImprimantes.Variables;
using System.Windows.Forms;
using System.Diagnostics;

namespace AppImprimantes.Views
{
    public partial class MainView : Form
    {
        #region Constantes de la vue
        public const int CGRIP = 16;
        public const int CCAPTION = 32;
        public const int BTNTOPBARMARGIN = 3;
        public const byte SIDEBARMARGINLEFT = 70;
        public const int SIDEBARCLIENTRELATIVEDIVISION = 18;
        public const byte INFOPANELBASESIZE = 36;
        public const byte BTNIFOWIDTH = 5;

        public const byte MARGINPNLBAR = 10;
        public const byte BTNNAVMARGINTOP = 0;
        public const byte PNLCONTENTMARGIN = 20;
        public const byte LBLPRICEMARGINRILEFT = 10;
        public const byte TRACKBARPRICEMARGINLEFT = 10;

        public const byte ARTICLENAMEMARGINLEFT = 50;
        #endregion

        private static Model localModel;
        public static Model LocalModel { get { return localModel; } set { localModel = value; } }
        private bool triChanged;

        private bool isSqlReady = false;

        public MainView()
        {
            InitializeComponent();
            SetupObjects();
            TestArticleAndSend(null,null);

        }

        private void SetupObjects()
        {
            SetupWindows();

            CreateImpInfoBtn();
            CreateCarInfoBtn();

            pnlImprimanteInfo.BringToFront();
            pnlConsommablesInfo.BringToFront();

            SetPrixTrackBar(Math.Min(priConMin, priImpMin), Math.Max(priConMax, priImpMax));

            SetComboBoxArticles(cbbArticlesText);

            SetComboBoxMarqueText();
            SetComboBoxMarque(cbbMarquesList);

            isSqlReady = true;
        }

        private void SetupWindows()
        {
            FormBorderStyle = FormBorderStyle.None;
            DoubleBuffered = true;
            SetStyle(ControlStyles.ResizeRedraw, true);
            MinimumSize = new Size(795, 450);
        }

        private void CreateImpInfoBtn()
        {
            btnImpInfo = new Button[btnImpCount];

            for (int i = 0; i < btnImpCount; i++)
            {
                btnImpInfo[i] = new Button();
                btnImpInfo[i].FlatStyle = FlatStyle.Flat;
                btnImpInfo[i].FlatAppearance.BorderSize = 0;
                btnImpInfo[i].BackColor = pnlImprimanteInfo.BackColor;
                btnImpInfo[i].Text = marquesImprimantes[i];
                btnImpInfo[i].Click += new EventHandler(PnlImpInfoBtn_Click);
                pnlConsommablesInfo.Controls.Add(btnImpInfo[i]);
                btnImpInfo[i].BringToFront();
            }
        }

        private void CreateCarInfoBtn()
        {
            btnConsommablesInfo = new Button[btnCartoucheCount];

            for (int i = 0; i < btnCartoucheCount; i++)
            {
                btnConsommablesInfo[i] = new Button();
                btnConsommablesInfo[i].FlatStyle = FlatStyle.Flat;
                btnConsommablesInfo[i].FlatAppearance.BorderSize = 0;
                btnConsommablesInfo[i].BackColor = pnlConsommablesInfo.BackColor;
                btnConsommablesInfo[i].Text = marquesConsomables[i];
                btnConsommablesInfo[i].Click += new EventHandler(PnlConInfoBtn_Click);
                pnlConsommablesInfo.Controls.Add(btnConsommablesInfo[i]);
                btnConsommablesInfo[i].BringToFront();
            }
        }

        private void SetPrixTrackBar(float priMin, float priMax)
        {
            priMin = priMin * 100;
            priMax = priMax * 100;

            int tempMin = Convert.ToInt32(priMin);
            int tempMax = Convert.ToInt32(priMax);

            trackBarMax.Minimum = tempMin;
            trackBarMax.Maximum = tempMax;

            //Apparait 2 fois pour activer la méthode valueChanged
            trackBarMax.Value = tempMax - 1;
            trackBarMax.Value = tempMax;

            trackBarMin.Minimum = tempMin;
            trackBarMin.Maximum = tempMax;

            //Apparait 2 fois pour activer la méthode valueChanged
            trackBarMin.Value = tempMin + 1;
            trackBarMin.Value = tempMin;
        }

        private void SetButtonImpInfo()
        {
            for (int i = 0; i < btnImpCount; i++)
            {
                btnImpInfo[i].Height = pnlImprimanteInfo.Height / btnImpCount;
                btnImpInfo[i].Top = (btnImpInfo[i].Height + 1) * i;
                btnImpInfo[i].Width = pnlImprimanteInfo.Width;
                pnlImprimanteInfo.Controls.Add(btnImpInfo[i]);
                btnImpInfo[i].BringToFront();
            }

        }

        private void SetButtonCartoucheInfo()
        {
            for (int i = 0; i < btnCartoucheCount; i++)
            {
                btnConsommablesInfo[i].Height = pnlConsommablesInfo.Height / btnCartoucheCount;
                btnConsommablesInfo[i].Top = (btnConsommablesInfo[i].Height + 1) * i;
                btnConsommablesInfo[i].Width = pnlConsommablesInfo.Width;
            }
        }

        private void SetComboBoxArticles(string[] arrayString)
        {
            if (cbbArticle.Items.Count != 0)
                cbbArticle.Items.Clear();

            foreach (string text in arrayString)
                cbbArticle.Items.Add(text);

            cbbArticle.SelectedItem = cbbArticle.Items[0];
        }

        private void SetComboBoxMarque(List<string> listString)
        {
            if (cbbMarque.Items.Count != 0)
                cbbMarque.Items.Clear();

            foreach (string text in listString)
                cbbMarque.Items.Add(text);

            cbbMarque.SelectedItem = cbbMarque.Items[0];
        }

        private void SetComboBoxMarqueText()
        {
            if (cbbArticle.SelectedItem != null)
            {
                cbbMarquesList.Clear();

                string tempSelectedArticle = cbbArticle.SelectedItem.ToString();

                if (tempSelectedArticle == cbbArticlesText[0])
                {
                    foreach (string text in marquesConsomables)
                    {
                        if (!cbbMarquesList.Contains(text))
                            cbbMarquesList.Add(text);
                    }
                    foreach (string text in marquesImprimantes)
                    {
                        if (!cbbMarquesList.Contains(text))
                            cbbMarquesList.Add(text);
                    }
                }
                else
                {
                    if (tempSelectedArticle == cbbArticlesText[1])
                    {
                        cbbMarquesList.AddRange(marquesImprimantes);
                    }
                    else
                    {
                        if (tempSelectedArticle == cbbArticlesText[2])
                        {
                            cbbMarquesList.AddRange(marquesConsomables);
                        }
                        else
                        {
                            if (tempSelectedArticle == cbbArticlesText[2])
                            {
                                cbbMarquesList.AddRange(marquesConsomables);
                            }
                        }
                    }
                }
            }
        }

        private void SetupComboBoxTris(Dictionary<string,string> dictionary)
        {
            HideArticle(null,null);

            if (cbbTrier.Items.Count != 0)
                cbbTrier.Items.Clear();

            foreach (string text in dictionary.Keys)
                cbbTrier.Items.Add(text);

            cbbTrier.SelectedItem = cbbTrier.Items[0];
        }

        public void AddFrames()
        {
            for (int i = 0; i < frameList.Count; i++)
            {
                flpFrames.Controls.Add(frameList[i].pnl);
                frameList[i].Click += Frame_Click;
            }
        }

        public void GetAllFrames()
        {
            SendAllRequest(this, null);
            SortFrames();
            AddFrames();
        }

        #region Mise à jour des positions des objets de la fenêtre
        /// <summary>
        /// Création du rectangle et du Grip
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            Rectangle rc = new Rectangle(ClientSize.Width - CGRIP, ClientSize.Height - CGRIP, CGRIP, CGRIP);
            ControlPaint.DrawSizeGrip(e.Graphics, BackColor, rc);
            Brush brush = new SolidBrush(Color.FromArgb(55, 55, 55));
            rc = new Rectangle(0, 0, ClientSize.Width, CCAPTION);
            e.Graphics.FillRectangle(brush, rc);
        }

        /// <summary>
        /// Mise à jour des positions 
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x84)
            {
                Point pos = new Point(m.LParam.ToInt32());
                pos = PointToClient(pos);
                if (pos.Y < CCAPTION)
                {
                    m.Result = (IntPtr)2;
                    return;
                }
                if (pos.X >= ClientSize.Width - CGRIP && pos.Y >= ClientSize.Height - CGRIP)
                {
                    m.Result = (IntPtr)17;
                    return;
                }
            }
            base.WndProc(ref m);

            //Boutons en haut à droite de la fenêtre
            btnClose.Left = ClientSize.Width - btnClose.Width - BTNTOPBARMARGIN;
            btnMaximize.Left = btnClose.Left - btnMaximize.Width - BTNTOPBARMARGIN;


            //Paneau de navigation à gauche
            pnlNavigationBar.Height = ClientSize.Height - CCAPTION;
            pnlNavigationBar.Width = SIDEBARMARGINLEFT + MinimumSize.Width / SIDEBARCLIENTRELATIVEDIVISION;
            pnlNavigationBar.Top = CCAPTION;

            //Paneau du menu
            pnlMenu.Height = pnlNavigationBar.Height - CGRIP;
            pnlMenu.Width = ClientSize.Width - pnlNavigationBar.Width;
            pnlMenu.Left = pnlNavigationBar.Width;

            //Flawlayout panel des produits
            flpFrames.Height = pnlMenu.Height;
            flpFrames.Width = pnlMenu.Width;

            //Boutons de navigations
            btnImprimantes.Width = pnlNavigationBar.Width;
            btnConsommables.Width = pnlNavigationBar.Width;
            btnAccount.Width = pnlNavigationBar.Width;
            btnConsommables.Top = btnImprimantes.Top + btnImprimantes.Height + BTNNAVMARGINTOP;
            btnAccount.Top = btnConsommables.Top + btnConsommables.Height + BTNNAVMARGINTOP;

            //Paneau de selection des imprimantes
            pnlImprimanteInfo.Width = INFOPANELBASESIZE * BTNIFOWIDTH + ClientSize.Width / (SIDEBARCLIENTRELATIVEDIVISION);
            pnlImprimanteInfo.Height = INFOPANELBASESIZE * btnImpCount + ClientSize.Width / (SIDEBARCLIENTRELATIVEDIVISION - btnImpCount + 1);
            pnlImprimanteInfo.Left = pnlNavigationBar.Width;
            pnlImprimanteInfo.Top = btnImprimantes.Top + CCAPTION;

            //Paneau de selection des cartouches
            pnlConsommablesInfo.Width = INFOPANELBASESIZE * BTNIFOWIDTH + ClientSize.Width / (SIDEBARCLIENTRELATIVEDIVISION);
            pnlConsommablesInfo.Height = INFOPANELBASESIZE * btnCartoucheCount + ClientSize.Width / (SIDEBARCLIENTRELATIVEDIVISION - btnCartoucheCount + 1);
            pnlConsommablesInfo.Left = pnlNavigationBar.Width;
            pnlConsommablesInfo.Top = btnConsommables.Top + CCAPTION;

            //Lignes plus foncées sur le paneau de selection des imprimantes
            pnlImpDrawBar.Height = pnlImprimanteInfo.Height;
            pnlImpDrawBar.Width = (pnlImprimanteInfo.Width / 100) * (100 - MARGINPNLBAR);
            pnlImpDrawBar.Left = (pnlImprimanteInfo.Width - pnlImpDrawBar.Width) / 2;

            //Lignes plus foncées sur le paneau de selection des cartouches
            pnlCartoucheDrawBar.Height = pnlConsommablesInfo.Height;
            pnlCartoucheDrawBar.Width = (pnlConsommablesInfo.Width / 100) * (100 - MARGINPNLBAR);
            pnlCartoucheDrawBar.Left = (pnlConsommablesInfo.Width - pnlCartoucheDrawBar.Width) / 2;

            //Paneau qui contient les frames
            pnlContent.Height = pnlMenu.Height - pnlSchearch.Height;
            pnlContent.Width = pnlMenu.Width - (PNLCONTENTMARGIN * 2);
            pnlContent.Left = PNLCONTENTMARGIN;
            pnlContent.Top = pnlSchearch.Top + pnlSchearch.Height;

            //Mise a jour de la position des trackbars
            trackBarMin.Left = cbbMarque.Left + cbbMarque.Width + TRACKBARPRICEMARGINLEFT;
            trackBarMax.Left = cbbArticle.Left + cbbArticle.Width + TRACKBARPRICEMARGINLEFT;

            //Panneau de recherche
            pnlSchearch.Width = pnlContent.Width;
            pnlSchearch.Left = pnlContent.Left;

            //Label des prix min et max des trackbars;
            lblPrixMin.Left = trackBarMin.Left + trackBarMin.Width + LBLPRICEMARGINRILEFT;
            lblPrixMax.Left = trackBarMax.Left + trackBarMax.Width + LBLPRICEMARGINRILEFT;

            //Paneau contenant les infos de l'article selectionné
            pnlArticle.Left = flpFrames.Width /2 - pnlArticle.Width/2;
            pnlArticle.Top = (pnlSchearch.Height + flpFrames.Height) / 2 - pnlArticle.Height / 2 - 50;
        }
        #endregion

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnMaximize_Click(object sender, EventArgs e)
        {
            if (maximized == false)
            {
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                WindowState = FormWindowState.Normal;
            }
            maximized = !maximized;
        }

        private void BtnSideBar_MouseHover(object sender, EventArgs e)
        {
            if (sender == btnConsommables || sender == pnlConsommablesInfo)
            {
                pnlConsommablesInfo.Visible = true;
                SetButtonCartoucheInfo();
            }
            else
                pnlConsommablesInfo.Visible = false;

            if (sender == btnImprimantes || sender == pnlImprimanteInfo)
            {
                pnlImprimanteInfo.Visible = true;
                SetButtonImpInfo();
            }
            else
                pnlImprimanteInfo.Visible = false;

        }

        private void TrackBarMin_ValueChanged(object sender, EventArgs e)
        {
            string tempPrice = trackBarMin.Value.ToString();

            lblPrixMin.Text = "Prix minimum : " + tempPrice.Substring(0, tempPrice.Length - 2) + "," + tempPrice.Substring(tempPrice.Length - 2, 2) + "Fr.";

            if (trackBarMin.Value >= trackBarMax.Value)
            {
                trackBarMax.Value = trackBarMin.Value;
            }

            minPriceChanged = true;
        }

        private void TrackBarMax_ValueChanged(object sender, EventArgs e)
        {
            string tempPrice = trackBarMax.Value.ToString();

            lblPrixMax.Text = "Prix maximum : " + tempPrice.Substring(0, tempPrice.Length - 2) + "," + tempPrice.Substring(tempPrice.Length - 2, 2) + "Fr.";

            if (trackBarMax.Value <= trackBarMin.Value)
            {
                trackBarMin.Value = trackBarMax.Value;
            }

            minPriceChanged = true;
        }

        private void CbbArticle_SelectedIndexChanged(object sender, EventArgs e)
        {
            triChanged = false;

            if (cbbArticle.SelectedItem.ToString() == cbbArticlesText[1])
            {
                SetupComboBoxTris(CommandesTrisImp);
            }
            else
            {
                if (cbbArticle.SelectedItem.ToString() == cbbArticlesText[2])
                {
                    SetupComboBoxTris(CommandesTrisCon);
                }
                else
                {
                    SetupComboBoxTris(CommandesTrisAll);
                }
            }

            SetComboBoxMarqueText();
            SetComboBoxMarque(cbbMarquesList);

            triChanged = true;
        }

        private void PnlConInfoBtn_Click(object sender, EventArgs e)
        {
            Button btnSender = (sender as Button);
            cbbArticle.SelectedItem = cbbArticle.Items[2];
            cbbMarque.SelectedItem = cbbMarque.Items[Array.IndexOf(btnConsommablesInfo, btnSender)];
            pnlConsommablesInfo.Visible = false;
            TestComboboxArticle(btnSender);
        }

        private void PnlImpInfoBtn_Click(object sender, EventArgs e)
        {
            Button btnSender = (sender as Button);
            cbbArticle.SelectedItem = cbbArticle.Items[1];
            cbbMarque.SelectedItem = cbbMarque.Items[Array.IndexOf(btnImpInfo, btnSender)];
            pnlImprimanteInfo.Visible = false;
            TestComboboxArticle(btnSender);
        }

        private void BtnImprimantes_Click(object sender, EventArgs e)
        {
            Button btnSender = (sender as Button);
            cbbArticle.SelectedItem = cbbArticle.Items[1];
            cbbMarque.SelectedItem = cbbMarque.Items[0];
            pnlImprimanteInfo.Visible = false;
            SetPrixTrackBar(priImpMin, priImpMax);
            SetupComboBoxTris(CommandesTrisImp);
        }

        private void BtnConsommables_Click(object sender, EventArgs e)
        {
            Button btnSender = (sender as Button);
            cbbArticle.SelectedItem = cbbArticle.Items[2];
            cbbMarque.SelectedItem = cbbMarque.Items[0];
            pnlConsommablesInfo.Visible = false;
            SetPrixTrackBar(priConMin, priConMax);
            SetupComboBoxTris(CommandesTrisCon);
        }

        public void TestComboboxArticle(object sender)
        {
            string btnSender = (sender as Button).Text;

            if (btnSender == btnImprimantes.Text)
                SetPrixTrackBar(priImpMin, priImpMax);
            else
            {
                if (btnSender == btnConsommables.Text)
                    SetPrixTrackBar(priConMin, priConMax);
            }
        }

        public void SendImpRequest(object sender, EventArgs e)
        {
            if (isSqlReady)
                localModel.GetImpByMarque(cbbMarque.SelectedItem.ToString(), CommandesTrisImp[cbbTrier.SelectedItem.ToString()], trackBarMin.Value/100, trackBarMax.Value/100);
        }

        public void SendConRequest(object sender, EventArgs e)
        {
            if (isSqlReady)
                localModel.GetConByMarque(cbbMarque.SelectedItem.ToString(), CommandesTrisCon[cbbTrier.SelectedItem.ToString()], trackBarMin.Value / 100, trackBarMax.Value / 100);
        }

        public void SendAllRequest(object sender, EventArgs e)
        {
            if (isSqlReady)
                localModel.GetConByMarque(cbbMarque.SelectedItem.ToString(), null, trackBarMin.Value / 100, trackBarMax.Value / 100);

            if (isSqlReady)
                localModel.GetImpByMarque(cbbMarque.SelectedItem.ToString(), null, trackBarMin.Value / 100, trackBarMax.Value / 100);
        }

        private void RemoveFramesAndList(ref List<Frame> frameList, ref FlowLayoutPanel flp)
        {
            foreach (Frame f in frameList)
                flp.Controls.Remove(f.pnl);

            frameList = new List<Frame>();
        }

        private void RemoveFrames(ref List<Frame> frameList, ref FlowLayoutPanel flp)
        {
            pnlContent.Controls.Clear();
        }

        private void TestArticleAndSend(object sender, EventArgs e)
        {
            RemoveFramesAndList(ref frameList, ref flpFrames);

            if (sender == cbbTrier && !triChanged) return;

            if (cbbArticle.SelectedItem.ToString() == cbbArticlesText[1])
            {
                SendImpRequest(sender, e);
                AddFrames();
            }
            else
            {
                if (cbbArticle.SelectedItem.ToString() == cbbArticlesText[2])
                {
                    SendConRequest(sender, e);
                    AddFrames();
                }
                else
                {
                    SendAllRequest(this,null);
                    SortFrames();
                    AddFrames();
                }
            }
        }

        private void TrackMinBar_ValueChanged(object sender, MouseEventArgs e)
        {
            if (minPriceChanged)
                TestArticleAndSend(sender, e);
        }

        public void Frame_Click(object sender, EventArgs e)
        {
            Frame article = sender as Frame;
            model.GetArticle(article.nom);

            if (articleInfo.Count != 0)
            {
                pnlArticle.Visible = true;

                string prix = article.prix.ToString();

                if (prix.Length - prix.ToString().IndexOf('.') > 2)
                    prix = prix.Substring(0, prix.ToString().IndexOf('.') + 3);

                pcbArticle.BackgroundImage = article.pbox.Image;
                lblAPrix.Text = "Prix : " + prix + " FR.";
                lblAName.Text = articleInfo[1];
                lblAMarque.Text = "Marque : " + articleInfo[2];
                lblARes.Text = "Resolution : " + articleInfo[3];
                lblAHeight.Text = "Hauteur : " + articleInfo[4];
                lblAWidth.Text = "Largeur : " + articleInfo[5];
                lblALenght.Text = "Longueur : " + articleInfo[6];
                lblASpeed.Text = "Impressions par minute : " + articleInfo[7];
                lblARelease.Text = "Date de sortie : " + articleInfo[8];
                lblAWeight.Text = "Poids : " + articleInfo[9];
                lblFormat.Text = "Format : " + articleInfo[10];
                lblAType.Text = "Format : " + articleInfo[11];
                lblACon.Text = "Consommable : " + articleInfo[12];
            }
        }

        private void SortFrames()
        {
            if (cbbTrier.Items.Count != 0)
            {
                if (cbbTrier.SelectedItem.ToString() == "Nom (A-Z)")
                    frameList = frameList.OrderBy(x => x.nom).ToList();
                if (cbbTrier.SelectedItem.ToString() == "Nom (Z-A)")
                    frameList = frameList.OrderBy(x => x.nom).ToList();
                if (cbbTrier.SelectedItem.ToString() == "Prix croissant")
                    frameList = frameList.OrderBy(x => x.prix).ToList();
                if (cbbTrier.SelectedItem.ToString() == "Prix décroissant")
                    frameList = frameList.OrderBy(x => -x.prix).ToList();
            }
        }

        private void HideArticle(object sender, EventArgs e)
        {
            pnlArticle.Visible = false;
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            //model.InsertCommandC(articleInfo[0] , model.session.ID.ToString());
        }
    }
}
