﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppImprimantes.Views
{
    public partial class Login : Form
    {
        bool tbLoginFocused = true;
        bool tbPasswordFocused = true;
        public Login()
        {
            InitializeComponent();
        }
        private void tbLogin_GotFocus(object sender, EventArgs e)
        {
            if (tbLoginFocused)
            {
                txt_EmailConnect.ResetText();
                tbLoginFocused = false;
            }

        }

        private void tbPassword_GotFocus(object sender, EventArgs e)
        {
            if (tbPasswordFocused)
            {
                txt_PasswordConnect.ResetText();
                tbPasswordFocused = false;
            }
            txt_PasswordConnect.UseSystemPasswordChar = true;
        }

        private void bConnection_Click(object sender, EventArgs e)
        {
            if (Variables.model.TryConnectUser(txt_EmailConnect.Text.ToLower(), txt_PasswordConnect.Text))
            {
                Hide();
                MainView view = new MainView();
                view.ShowDialog();
                Application.Exit();
            }
            else
            {
                txt_PasswordConnect.Clear();
                MessageBox.Show("Adresse email ou mot de passe invalide!");
            }
        }

        private void bRegister_Click(object sender, EventArgs e)
        {
            Register register = new Register(this);
            register.Show();

            Hide();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblExit_MouseEnter(object sender, EventArgs e)
        {
            lblExit.BackColor = Color.LightGray;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            lblExit.BackColor = Color.Transparent;
        }
    }
}
