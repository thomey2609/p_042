﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppImprimante
{
    public class Session
    {
        private bool connected = false;
        private string name = "";
        private string surname = "";
        private string email = "";
        private int _id;

        public Session()
        {

        }
        public bool IsConnected { get { return connected; } }
        public string Name {get { return name;} }
        public string Surname {get { return surname; } }
        public string Email {get { return surname; } }
        public int ID { get { return _id; } }
        public void Connection(string _name , string _sureName , string _email , int id)
        {
            connected = true;
            name = _name;
            surname = _sureName;
            email = _email;
            _id = id;
        }
        public void Disconnect()
        {
            connected = false;
            name = "";
            surname = "";
            email = "";
        }
    }

}
