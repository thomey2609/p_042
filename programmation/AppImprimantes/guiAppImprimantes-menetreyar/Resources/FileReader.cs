﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;

namespace AppImprimantes
{
    public class FileReader
    {
        Dictionary<string, string> Dictionary = new Dictionary<string, string>();

        string fileName;
        string line;
        char collumnSeparator = ';';

        public FileReader(string fileName)
        {
            this.fileName = fileName;
        }

        public Dictionary<string, string> getFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            string resourceName = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith(fileName));

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))

            using (StreamReader reader = new StreamReader(stream))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    int keyLenght = line.IndexOf(collumnSeparator);
                    Dictionary.Add(line.Substring(0, keyLenght), line.Substring(keyLenght + 1, line.Length - keyLenght - 1));
                }
            }

            return Dictionary;
        }

        public List<string> getValues()
        {
            if (Dictionary.Count == 0) getFile();

            List<string> listKeys = new List<string>();

            foreach (KeyValuePair<string, string> item in Dictionary)
            {
                listKeys.Add(item.Value);
            }

            return listKeys;
        }

        public List<string> getKeys()
        {
            if (Dictionary.Count == 0) getFile();

            List<string> listNames = new List<string>();

            foreach (KeyValuePair<string, string> item in Dictionary)
            {
                listNames.Add(item.Key);
            }

            return listNames;
        }
    }
}
