﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppImprimantes
{
    public static class Variables
    {
        public static Button[] btnImpInfo;          //Boutons pour le choix de la marque des imprimantes
        public static Button[] btnConsommablesInfo; //Boutons pour le choix de la marque des consommables

        public static List<string> marquesImprimantes = new List<string>(); //Nom des marques des imprimantes
        public static List<string> marquesConsomables = new List<string>(); //Nom des marques des consommables

        public static float priConMin;  //Prix minimum des consommables
        public static float priConMax;  //Prix maximum des consommables

        public static float priImpMin;  //Prix minimum des imprimantes
        public static float priImpMax;  //Prix maximum des imprimantes

        public static string[] cbbArticlesText = new string[] { "Tous", "Imprimantes", "Consommables" };//Combobox du choix du produit
        public static List<string> cbbMarquesList = new List<string>();                             //Liste des marques

        public static Dictionary<string, string> CommandesTrisAll = new Dictionary<string, string>();  //Dictionnaires des commandes de tris des imprimantes (Clé : Nom)
        public static Dictionary<string,string> CommandesTrisImp = new Dictionary<string,string>();    //Dictionnaires des commandes de tris des imprimantes (Clé : Nom, Valeur : Commande)
        public static Dictionary<string, string> CommandesTrisCon = new Dictionary<string, string>();  //Dictionnaires des commandes de tris des consommables (Clé : Nom, Valeur : Commande)

        public static string[] btnImpText;

        public static Model model = new Model();
        public static List<string> articleInfo = new List<string>();

        public static bool maximized = false;

        public static int btnImpCount;
        public static int btnCartoucheCount;

        public static List<string> impName = new List<String>();

        public static List<Frame> frameList = new List<Frame>();

        public static bool minPriceChanged;
        public static bool maxPriceChanged;

    }
}
