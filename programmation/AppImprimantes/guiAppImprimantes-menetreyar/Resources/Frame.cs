﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace AppImprimantes
{
    public class Frame : Control
    {
        public Panel pnl { get; } = new Panel();
        public PictureBox pbox { get; private set; }  = new PictureBox();

        private Label lblNom = new Label();
        private Label lblPrix = new Label();

        public double prix { get; private set; }
        public string nom { get; private set; }

        public int impHeight { get; private set; }
        public int impWidth { get; private set; }
        public int impLenght { get; private set; }

        new public event EventHandler Click;

        private string command;
        private Model model;

        public Frame(Bitmap bmp, string str, double prix, Model model, int lenght, int width, int height)
        {
            this.model = model;
            impHeight = height;
            impLenght = lenght;
            impWidth = width;

            SetupPanel();
            SetupImage(bmp);
            SetupLabel(str, prix);
            SetEvents();
        }

        protected virtual void OnClick(EventArgs e)
        {
            EventHandler handler = Click;
            handler?.Invoke(this, e);
        }

        private void OnComponentClick(object sender, EventArgs e)
        {
            OnClick(null);
        }

        public Frame(string str, double prix, Model model)
        {
            this.model = model;
            SetupPanel();
            SetupLabel(str, prix);
            SetEvents();
        }

        private void SetEvents()
        {
            pnl.MouseUp += Frame_ClickExit;
            pnl.Click += OnComponentClick;
            pnl.MouseEnter += Frame_Hover;
            pnl.MouseLeave += Frame_Exit;
            pnl.MouseDown += Frame_Pressed;

            foreach (Control ctr in pnl.Controls)
            {
                ctr.MouseUp += Frame_ClickExit;
                ctr.Click += OnComponentClick;
                ctr.MouseEnter += Frame_Hover;
                ctr.MouseLeave += Frame_Exit;
                ctr.MouseDown += Frame_Pressed;
            }
        }

        private void SetupLabel(string str, double prix)
        {
            this.prix = prix;
            nom = str;

            lblNom.AutoSize = true;
            lblNom.ForeColor = Color.White;
            lblNom.Location = new Point(0, pbox.Top + pbox.Height + 3);
            lblNom.Size = new Size(pnl.Width, 40);
            lblNom.TextAlign = ContentAlignment.TopCenter;
            lblNom.Font = new Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));
            lblNom.ForeColor = Color.Turquoise;
            lblNom.Text = str;
            lblNom.AutoSize = false;

            int index = (prix.ToString()).IndexOf('.');

            if ((prix.ToString()).IndexOf('.') != -1)
            {
                if (prix.ToString().Length - 3 < index)
                    lblPrix.Text = prix.ToString().Substring(0, (prix.ToString().Length - index + index)) + " Fr.";
                else
                    lblPrix.Text = prix.ToString().Substring(0, (index + 3)) + " Fr.";
            }
            else
                lblPrix.Text = prix.ToString() + " Fr.-";

            lblPrix.AutoSize = true;
            lblPrix.ForeColor = Color.White;
            lblPrix.Location = new Point(6, lblNom.Top + lblNom.Height);
            lblPrix.Size = new Size(pnl.Width, 20);
            lblPrix.TextAlign = ContentAlignment.TopLeft;
            lblPrix.Font = new Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            lblPrix.AutoSize = false;
            pnl.Controls.Add(lblNom);
            pnl.Controls.Add(lblPrix);
        }

        private void SetupPanel()
        {

            pnl.Location = new Point(0, 0);
            pnl.Size = new Size(120, 180);
            pnl.BorderStyle = BorderStyle.FixedSingle;
            pnl.BackColor = Color.FromArgb(85, 85, 84);
        }

        private void SetupImage(Bitmap bmp)
        {
            pbox.Location = new Point(8, 8);
            pbox.Size = new Size(pnl.Width- 2 * 8, pnl.Width - 2 * 8);
            pbox.TabStop = false;
            pbox.Image = bmp;
            pbox.SizeMode = PictureBoxSizeMode.StretchImage;

            pnl.Controls.Add(pbox);
        }

        private void Frame_Pressed(object sender, EventArgs e)
        {
            pnl.BackColor = pnl.BackColor = Color.FromArgb(155, 155, 154);
        }

        private void Frame_Hover(object sender, EventArgs e)
        {
            pnl.BackColor = pnl.BackColor = Color.FromArgb(105, 105, 104);
        }

        private void Frame_Exit(object sender, EventArgs e)
        {
            pnl.BackColor = pnl.BackColor = Color.FromArgb(85, 85, 84);
        }

        private void Frame_ClickExit(object sender, EventArgs e)
        {
            pnl.BackColor = pnl.BackColor = Color.FromArgb(105, 105, 104);
        }

    }
}
