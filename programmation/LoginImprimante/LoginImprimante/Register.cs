﻿using System;
using System.Windows.Forms;

namespace guiAppImprimantes_menetreyar
{
    public partial class Register : Form
    {
        private bool gotFocusedSurname = true;
        private bool gotFocusedName = true;
        private bool gotFocusedEmail = true;
        private bool gotFocusedPassword = true;
        private bool gotFocusedConfirmation = true;
        private Login login;

        public Register(Login login)
        {
            InitializeComponent();
            this.login = login;

        }
        private void tbSurname_GotFocus(object sender, EventArgs e)
        {
            if (gotFocusedSurname)
            {
                txt_name.ResetText();
                gotFocusedSurname = false;
            }
        }
        private void tbName_GotFocus(object sender, EventArgs e)
        {
            if (gotFocusedName)
            {
                txt_surname.ResetText();
                gotFocusedName = false;
            }
        }
        private void tbEmail_GotFocus(object sender, EventArgs e)
        {
            if (gotFocusedEmail)
            {
                txt_email.ResetText();
                gotFocusedEmail = false;
            }
        }
        private void tbPassword_GotFocus(object sender, EventArgs e)
        {
            if (gotFocusedPassword)
            {
                txt_password.ResetText();
                gotFocusedPassword = false;
            }
            txt_password.UseSystemPasswordChar = true;
        }
        private void tbConfirmation_GotFocus(object sender, EventArgs e)
        {
            if (gotFocusedConfirmation)
            {
                txt_password2.ResetText();
                gotFocusedConfirmation = false;
            }
            txt_password2.UseSystemPasswordChar = true;
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {

        }

        private void btnConnection_Click(object sender, EventArgs e)
        {
            Close();
            login.Show();
        }
    }
}
