﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guiAppImprimantes_menetreyar
{
    public partial class Login : Form
    {
        bool tbLoginFocused = true;
        bool tbPasswordFocused = true;

        public Login()
        {
            InitializeComponent();
        }
        private void tbLogin_GotFocus(object sender, EventArgs e)
        {
            if (tbLoginFocused)
            {
                tbEmail.ResetText();
                tbLoginFocused = false;
            }
                
        }

        private void tbPassword_GotFocus(object sender, EventArgs e)
        {
            if (tbPasswordFocused)
            {
                tbPassword.ResetText();
                tbPasswordFocused = false;
            }
            tbPassword.UseSystemPasswordChar = true;
        }

        private void bConnection_Click(object sender, EventArgs e)
        {

        }

        private void bRegister_Click(object sender, EventArgs e)
        {
            Register register = new Register(this);
            register.Show();

            // à changer psk enfait c la form de base ducoup on p eut pas la quitter
            Hide();
        }
    }
}
