-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 09 Octobre 2019 à 13:16
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `p042database`
--
CREATE DATABASE IF NOT EXISTS `p042database` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `p042database`;

-- --------------------------------------------------------

--
-- Structure de la table `t_client`
--

CREATE TABLE `t_client` (
  `id_Client` int(11) NOT NULL,
  `cliName` varchar(50) NOT NULL,
  `cliSurname` varchar(50) NOT NULL,
  `cliRegisterYears` date NOT NULL,
  `cliEmail` varchar(50) NOT NULL,
  `cliPassword` varchar(50) NOT NULL,
  `id_printer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_client`
--

INSERT INTO `t_client` (`id_Client`, `cliName`, `cliSurname`, `cliRegisterYears`, `cliEmail`, `cliPassword`, `id_printer`) VALUES
(2, 'Boris', 'Jaccard', '2019-10-09', 'Boris@BorisEnterprise.com', 'BorisLeMeilleurs123', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `t_consomable`
--

CREATE TABLE `t_consomable` (
  `id_Consomable` int(11) NOT NULL,
  `conMark` varchar(50) NOT NULL,
  `conModele` varchar(50) NOT NULL,
  `conPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_consomable`
--

INSERT INTO `t_consomable` (`id_Consomable`, `conMark`, `conModele`, `conPrice`) VALUES
(1, 'Brother TN', '243BK - Noir', 55.5),
(2, 'HP', '302 - Noir', 14.5),
(3, 'HP ', '304 - Noir', 15),
(4, 'Brother', 'LC3211BK - Noir', 15.8),
(5, 'Brother TN', '243BK - Noir', 57.9),
(6, 'OKI ', 'Toner 5k - Noir', 85.9),
(7, 'Brother ', 'DR-2400 - Noir', 23.9),
(8, 'Epson ', '106 - Noir', 12.95),
(9, 'Epson ', '664 Ecotank Black ink bottle', 10.9),
(10, 'EPSON ', 'C13T664140 - Noir', 14.95),
(11, 'HP', ' 205A - Noir', 47.15),
(12, 'HP ', '304 - Noir', 15),
(13, 'HP ', '304 - Noir', 15),
(14, 'Canon ', 'PGI 570XL PGBK - Noir', 22.95),
(15, 'Brother TN', '2410 - Noir', 56.99),
(16, 'Canon ', 'PG-545 8ml - Noir', 24.8),
(17, 'HP ', '303 - Noir', 21.95),
(18, 'Brother TN', '423BK', 101),
(19, 'Brother', ' LC-3219XLBK', 39),
(20, 'Epson ', 'Singlepack Black 35 DURABrite', 33.9),
(21, 'Samsung ', 'MLT-D111S - Noir', 50.9),
(22, 'HP ', '711 - Noir', 34.3),
(23, 'Brother TN', '243BK - Noir ', 54.4),
(24, 'HP ', '62 - Noir', 14.95),
(25, 'HP ', '962 - Noir', 32.99),
(26, 'Canon ', 'CLI-581BK - Noir', 12.5),
(27, 'HP', '410A', 73.7),
(28, 'Brother TN', '2410 - Noir', 56.99),
(29, 'Brother TN', '243BK - Noir', 69),
(30, 'Brother TN', '421BK - Noir', 110);

-- --------------------------------------------------------

--
-- Structure de la table `t_price`
--

CREATE TABLE `t_price` (
  `id_EvolutionPrice` int(11) NOT NULL,
  `evYears` int(11) NOT NULL,
  `evPrice` float NOT NULL,
  `id_printer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_price`
--

INSERT INTO `t_price` (`id_EvolutionPrice`, `evYears`, `evPrice`, `id_printer`) VALUES
(1, 2019, 105.9, 1),
(2, 2019, 29.95, 2),
(3, 2019, 69.9, 3),
(4, 2019, 99.9, 4),
(5, 2019, 219.9, 5),
(6, 2019, 998.9, 6),
(7, 2019, 169.9, 7),
(8, 2019, 709, 8),
(9, 2019, 199, 9),
(10, 2019, 1579.9, 10),
(11, 2019, 279.9, 11),
(12, 2019, 99, 12),
(13, 2019, 39.95, 13),
(14, 2019, 89.95, 14),
(15, 2019, 61.9, 15),
(16, 2019, 80.9, 16),
(17, 2019, 129.9, 17),
(18, 2019, 99.9, 18),
(19, 2019, 70.8, 19),
(20, 2019, 279.9, 20),
(21, 2019, 209, 21),
(22, 2019, 149.9, 22),
(23, 2019, 72, 23),
(24, 2019, 295.9, 24),
(25, 2019, 199, 25),
(26, 2019, 139.9, 26),
(27, 2019, 329.9, 27),
(28, 2019, 129.9, 28),
(29, 2019, 399, 29),
(30, 2019, 428, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_printer`
--

CREATE TABLE `t_printer` (
  `id_printer` int(11) NOT NULL,
  `priModele` char(50) NOT NULL,
  `priMark` char(50) NOT NULL,
  `priWidth` int(11) NOT NULL,
  `priHeight` int(11) NOT NULL,
  `priLenght` int(11) NOT NULL,
  `priType` varchar(50) NOT NULL,
  `priWeight` int(11) NOT NULL,
  `priPrintSpeed` int(11) NOT NULL,
  `priPaper` varchar(4) NOT NULL,
  `priResolution` varchar(20) NOT NULL,
  `priReleaseDate` varchar(20) NOT NULL,
  `priLink` varchar(200) NOT NULL,
  `id_Consomable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_printer`
--

INSERT INTO `t_printer` (`id_printer`, `priModele`, `priMark`, `priWidth`, `priHeight`, `priLenght`, `priType`, `priWeight`, `priPrintSpeed`, `priPaper`, `priResolution`, `priReleaseDate`, `priLink`, `id_Consomable`) VALUES
(1, 'HL-L3210CW', 'Brother', 461, 410, 252, 'Couleur', 18, 18, 'A4', '2400 x 600', '2018', 'https://www.brack.ch/fr/brother-imprimante-hl-l3210cw-798350?query=imprimantes&page=3', NULL),
(2, 'Officejet 3833', 'HP', 450, 224, 364, 'Couleur', 6, 20, 'DL,B', '4800 x 1200', '2019', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-officejet-3833-couleur-wifi--p0001407477', NULL),
(3, 'Deskjet 2620 white', 'HP', 304, 425, 149, 'Couleur', 3, 5, 'A4', '4800 x 1200', '2017', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/hp/deskjet-2620-white-8430302.html', NULL),
(4, 'DCP-J572DW', 'Brother', 228, 457, 477, 'Couleur', 7, 6, 'A4', '6000 x 1200', '2019', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/brother/dcp-j572dw-8371184.html', NULL),
(5, 'HL-L3270CDW', 'Brother', 439, 252, 461, 'Couleur', 19, 24, 'A4', '2400 x 600', '2018', 'https://www.steg-electronics.ch/fr/article/Brother-HL-L3270CDW-25246487.aspx', NULL),
(6, 'C844dnw', 'Oki Electric Industry', 449, 360, 552, 'Couleur', 40, 36, 'A3', '1200 x 1200', '2019', 'https://www.steg-electronics.ch/fr/article/OKI-C844dnw-25906454.aspx', NULL),
(7, 'HL-L2375DW', 'Brother', 356, 183, 360, 'Mono', 7, 34, 'A4', '1200 x 1200', '2018', 'https://www.steg-electronics.ch/fr/article/Brother-HL-L2375DW-24995873.aspx', NULL),
(8, 'Epson EcoTank ET-7750', 'Epson', 415, 526, 168, 'Couleur', 11, 26, 'A3+', '5760 x 1440', '2018', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/epson/ecotank-et-7750-8336892.html', NULL),
(9, 'EcoTank ET-2600', 'Epson', 445, 169, 304, 'Couleur', 5, 15, 'A4', '5760 x 1440', '2017', 'https://www.melectronics.ch/fr/p/797277300000/epson-ecotank-et-2600?gclid=CjwKCAjwtuLrBRAlEiwAPVcZBnEx0J65rpG4VU5ulIw6uEmzEVuCytkmW72BtStV7vvS10SaMH0RghoClH4QAvD_BwE&gclsrc=aw.ds', NULL),
(10, 'WorkForce Pro WF-8090 D3TWC', 'Epson', 615, 1045, 794, 'Couleur', 79, 34, 'A3+', '4800 x 1200', '2018', 'https://www.steg-electronics.ch/fr/article/Epson-WorkForce-Pro-WF-8090-D3TWC-19216379.aspx', NULL),
(11, 'Laserjet Pro Color M181fw', 'HP', 380, 420, 341, 'Couleur', 16, 16, 'A4', '600 x 600', '2018', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/hp/laserjet-pro-color-m181fw-8325499.html', NULL),
(12, 'HP ENVY 5030', 'HP', 128, 445, 564, 'Couleur', 5, 20, 'A4,A', '4800 x 1200', '2018', 'https://www.mediamarkt.ch/fr/product/_hp-envy-5030-1810416.html?ga_query=HP+ENVY+5030', NULL),
(13, 'Deskjet 2620 Blanc', 'HP', 304, 425, 149, 'Couleur', 3, 5, 'A4', '4800 x 1200', '2018', 'https://www.conforama.ch/fr/multimedia/informatique/imprimante/imprimante-hp-deskjet-2620-1n01b0-blanc/p/730826', NULL),
(14, 'CANON Pixma TS5055', 'Canon', 126, 372, 315, 'Couleur', 6, 12, 'A4,A', '4800x 1200', '2018', 'https://www.mediamarkt.ch/fr/product/_canon-pixma-ts5055-1710577.html', NULL),
(15, 'DCP-L2530DW', 'Brother', 410, 272, 398, 'Mono', 10, 30, 'A4', '2400 x 600', '2018', 'https://www.melectronics.ch/fr/p/797280500000/brother-dcp-l2530dw?gclid=CjwKCAjwtuLrBRAlEiwAPVcZBh4cZZLCeerTbTUOTXvTZK0aQZFHGBz-M8VSJjVCWciCzI-JRybyYBoC2xQQAvD_BwE&gclsrc=aw.ds', NULL),
(16, 'Pixma TR4550', 'Canon', 435, 180, 295, 'Couleur', 6, 8, 'A4', '4800 x 1200', '2018', 'https://www.melectronics.ch/fr/p/785300138320/canon-pixma-tr4550', NULL),
(17, 'HP Tango X', 'HP', 389, 260, 615, 'Couleur', 3, 11, 'DL,B', '4800 x 1200', '2018', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-tango-x-couleur-wlan--p0001560608', NULL),
(18, 'MFC-L8900CDW', 'Brother', 526, 549, 495, 'Couleur', 29, 32, 'A4,A', '2400 x 600', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l8900cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-6178338', NULL),
(19, 'MFC-J5730DW', 'Brother', 398, 374, 530, 'Couleur', 21, 35, 'A3,A', '4800 x 1200', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-j5730dw-wifi-encre-couleur-impression-recto-verso-imprimantes-6068739', NULL),
(20, 'EPSON WorkForcePro WF-4740DTWF', 'Epson', 425, 330, 388, 'Couleur', 12, 24, 'A4', '4800 x 1200', '2017', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/imprimante-multifonction-epson-workforcepro-wf-4740dtwf--p0001317537', NULL),
(21, 'Color Laser MFP 178nw', 'HP', 406, 288, 363, 'Couleur', 13, 4, 'A4', '600 x 600', '2019', 'https://www.brack.ch/fr/hp-multifunktionsdrucker-color-laser-mfp-178nw-956819?query=drucker&forceLanguageChange', NULL),
(22, '"T125, 24"" DesignJet"', 'HP', 987, 285, 255, 'Couleur', 26, 45, 'A1,A', '1200 x 1200', '2019', 'https://www.digitec.ch/fr/s1/product/hp-t125-24-designjet-wifi-encre-couleur-imprimantes-11301355', NULL),
(23, 'DCP-L3550CDW', 'Brother', 410, 414, 475, 'Couleur', 23, 18, 'A4,A', '2400 x 1200', '2018', 'https://www.digitec.ch/fr/s1/product/brother-dcp-l3550cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-9418839', NULL),
(24, 'OfficeJet 250 Mobile', 'HP', 380, 91, 198, 'Couleur', 3, 10, 'A4', '1200 x 1200 ', '2017', 'https://www.digitec.ch/fr/s1/product/hp-officejet-250-mobile-all-in-one-wifi-encre-couleur-imprimantes-5885640', NULL),
(25, 'OfficeJet Pro 9015', 'HP', 439, 278, 342, 'Couleur', 9, 18, 'A4', '4800 x 1200', '2019', 'https://www.brack.ch/fr/hp-multifunktionsdrucker-officejet-pro-9015-all-in-one-951542?query=imprimante+all+in+one&redirected=1', NULL),
(26, 'CANON PIXMA TS6250', 'Canon', 372, 139, 315, 'Couleur', 6, 15, 'A4', '4800 x 1200', '2018', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/imprimante-multifonctions-canon-pixma-ts6250--p0001559967', NULL),
(27, 'M477fdw Color LaserJet Pro', 'HP', 416, 400, 472, 'Couleur', 23, 27, 'A4,A', '600 x 600 ', '2019', 'https://www.digitec.ch/fr/s1/product/hp-m477fdw-color-laserjet-pro-wifi-laserled-couleur-impression-recto-verso-imprimantes-5622697', NULL),
(28, 'HL-L2350DW', 'Brother', 356, 183, 360, 'Mono', 7, 30, 'A5,A', '600 x 600', '2017', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/brother-hl-l2350dw-laser-led-noir-et-blanc--p0001430682', NULL),
(29, 'MFC-L3770CDW', 'Brother', 509, 410, 414, 'Couleur', 25, 24, 'A4,A', '600 x 600', '2018', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l3770cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-9421947', NULL),
(30, 'MFC-L8690CDW', 'Brother', 526, 539, 435, 'Couleur', 28, 28, 'A4', '1200 x 600', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l8690cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-6178083', NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD PRIMARY KEY (`id_Client`),
  ADD UNIQUE KEY `ID_t_Client_IND` (`id_Client`),
  ADD KEY `id_printer` (`id_printer`);

--
-- Index pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  ADD PRIMARY KEY (`id_Consomable`),
  ADD UNIQUE KEY `ID_t_consomable_IND` (`id_Consomable`);

--
-- Index pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD PRIMARY KEY (`id_EvolutionPrice`),
  ADD UNIQUE KEY `ID_t_Price_IND` (`id_EvolutionPrice`),
  ADD KEY `FKAvoir_IND` (`id_printer`);

--
-- Index pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD PRIMARY KEY (`id_printer`),
  ADD UNIQUE KEY `ID_t_Printer_IND` (`id_printer`),
  ADD KEY `id_Consomable` (`id_Consomable`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_client`
--
ALTER TABLE `t_client`
  MODIFY `id_Client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  MODIFY `id_Consomable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `t_price`
--
ALTER TABLE `t_price`
  MODIFY `id_EvolutionPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `t_printer`
--
ALTER TABLE `t_printer`
  MODIFY `id_printer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD CONSTRAINT `t_client_ibfk_1` FOREIGN KEY (`id_printer`) REFERENCES `t_printer` (`id_printer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD CONSTRAINT `FKAvoir_FK` FOREIGN KEY (`id_printer`) REFERENCES `t_printer` (`id_printer`);

--
-- Contraintes pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD CONSTRAINT `t_printer_ibfk_1` FOREIGN KEY (`id_Consomable`) REFERENCES `t_consomable` (`id_Consomable`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
