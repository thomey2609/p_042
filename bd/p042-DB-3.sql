-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 30 Octobre 2019 à 14:13
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `p042database`
--
CREATE DATABASE IF NOT EXISTS `p042database` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `p042database`;

-- --------------------------------------------------------

--
-- Structure de la table `t_client`
--

CREATE TABLE `t_client` (
  `id_Client` int(11) NOT NULL,
  `cliName` varchar(50) NOT NULL,
  `cliSurname` varchar(50) NOT NULL,
  `cliRegisterYears` date NOT NULL,
  `cliEmail` varchar(50) NOT NULL,
  `cliPassword` varchar(50) NOT NULL,
  `id_printer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_consomable`
--

CREATE TABLE `t_consomable` (
  `id_Consomable` int(11) NOT NULL,
  `conMark` varchar(50) NOT NULL,
  `conModele` varchar(50) NOT NULL,
  `conPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_price`
--

CREATE TABLE `t_price` (
  `id_EvolutionPrice` int(11) NOT NULL,
  `evYears` int(11) NOT NULL,
  `evPrice` float NOT NULL,
  `id_printer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_printer`
--

CREATE TABLE `t_printer` (
  `id_printer` int(11) NOT NULL,
  `priModele` char(50) NOT NULL,
  `priMark` char(50) NOT NULL,
  `priWidth` int(11) NOT NULL,
  `priHeight` int(11) NOT NULL,
  `priLenght` int(11) NOT NULL,
  `priType` varchar(50) NOT NULL,
  `priWeight` int(11) NOT NULL,
  `priPrintSpeed` int(11) NOT NULL,
  `priPaper` varchar(4) NOT NULL,
  `priResolution` varchar(20) NOT NULL,
  `priReleaseDate` varchar(20) NOT NULL,
  `priLink` varchar(200) NOT NULL,
  `id_Consomable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD PRIMARY KEY (`id_Client`),
  ADD UNIQUE KEY `ID_t_Client_IND` (`id_Client`),
  ADD KEY `id_printer` (`id_printer`);

--
-- Index pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  ADD PRIMARY KEY (`id_Consomable`),
  ADD UNIQUE KEY `ID_t_consomable_IND` (`id_Consomable`);

--
-- Index pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD PRIMARY KEY (`id_EvolutionPrice`),
  ADD UNIQUE KEY `ID_t_Price_IND` (`id_EvolutionPrice`),
  ADD KEY `FKAvoir_IND` (`id_printer`);

--
-- Index pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD PRIMARY KEY (`id_printer`),
  ADD UNIQUE KEY `ID_t_Printer_IND` (`id_printer`),
  ADD KEY `id_Consomable` (`id_Consomable`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_client`
--
ALTER TABLE `t_client`
  MODIFY `id_Client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  MODIFY `id_Consomable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `t_price`
--
ALTER TABLE `t_price`
  MODIFY `id_EvolutionPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `t_printer`
--
ALTER TABLE `t_printer`
  MODIFY `id_printer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD CONSTRAINT `t_client_ibfk_1` FOREIGN KEY (`id_printer`) REFERENCES `t_printer` (`id_printer`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD CONSTRAINT `FKAvoir_FK` FOREIGN KEY (`id_printer`) REFERENCES `t_printer` (`id_printer`);

--
-- Contraintes pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD CONSTRAINT `t_printer_ibfk_1` FOREIGN KEY (`id_Consomable`) REFERENCES `t_consomable` (`id_Consomable`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
