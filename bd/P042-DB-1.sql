-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Wed Sep 25 15:46:43 2019 
-- ********************************************* 


-- Database Section
-- ________________ 

create database p042database;
use p042database;


-- Tables Section
-- _____________ 

create table t_Client (
     id_Client int not null auto_increment,
     cliName varchar(50) not null,
     cliSurname varchar(50) not null,
     cliRegisterYears date not null,
     cliEmail varchar(50) not null,
     cliPurchasedPrinter varchar(50) not null,
     cliPurchasedPrinterPrice int not null,
     cliBillNumber int not null,
     cliSaleDate date not null,
     cliGuaranteeDate date not null,
     constraint ID_t_Client_ID primary key (id_Client));

create table t_consomable (
     id_Consomable int not null auto_increment,
     conMark varchar(50) not null,
     conPrice int not null,
     id_printer int,
     constraint ID_t_consomable_ID primary key (id_Consomable));

create table t_Price (
     id_EvolutionPrice int not null auto_increment,
     evYears int not null,
     evPrice int not null,
     id_printer int not null,
     constraint ID_t_Price_ID primary key (id_EvolutionPrice));

create table t_Printer (
     id_printer int not null auto_increment,
     priModele char(50) not null,
     priMark char(50) not null,
     priWidth int not null,
     priHeight int not null,
     priLenght int not null,
     priType varchar(50) not null,
     priWeight int not null,
     priPrintSpeed int not null,
     priPaper varchar(4) not null,
     priResolution varchar(20) not null,
     priReleaseDate date not null,
     priLink varchar(200) not null,
     id_Client int,
     constraint ID_t_Printer_ID primary key (id_printer));


-- Constraints Section
-- ___________________ 

alter table t_consomable add constraint FKConsomer_FK
     foreign key (id_printer)
     references t_Printer (id_printer);

alter table t_Price add constraint FKAvoir_FK
     foreign key (id_printer)
     references t_Printer (id_printer);

-- Not implemented
-- alter table t_Printer add constraint ID_t_Printer_CHK
--     check(exists(select * from t_Price
--                  where t_Price.id_printer = id_printer)); 

alter table t_Printer add constraint FKAcheter_FK
     foreign key (id_Client)
     references t_Client (id_Client);


-- Index Section
-- _____________ 

create unique index ID_t_Client_IND
     on t_Client (id_Client);

create unique index ID_t_consomable_IND
     on t_consomable (id_Consomable);

create index FKConsomer_IND
     on t_consomable (id_printer);

create unique index ID_t_Price_IND
     on t_Price (id_EvolutionPrice);

create index FKAvoir_IND
     on t_Price (id_printer);

create unique index ID_t_Printer_IND
     on t_Printer (id_printer);

create index FKAcheter_IND
     on t_Printer (id_Client);

