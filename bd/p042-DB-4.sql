-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 06 Novembre 2019 à 08:58
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `p042database`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_client`
--

CREATE TABLE `t_client` (
  `id_Client` int(11) NOT NULL,
  `cliName` varchar(50) NOT NULL,
  `cliSurname` varchar(50) NOT NULL,
  `cliRegisterYears` date NOT NULL,
  `cliEmail` varchar(50) NOT NULL,
  `cliPassword` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_consomable`
--

CREATE TABLE `t_consomable` (
  `id_Consomable` int(11) NOT NULL,
  `conMark` varchar(50) NOT NULL,
  `conModele` varchar(50) NOT NULL,
  `conPrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_consomable`
--

INSERT INTO `t_consomable` (`id_Consomable`, `conMark`, `conModele`, `conPrice`) VALUES
(1, 'Brother TN', '243BK - Noir', 55.5),
(2, 'HP', '302 - Noir', 14.5),
(3, 'HP ', '304 - Noir', 15),
(4, 'Brother', 'LC3211BK - Noir', 15.8),
(5, 'Brother TN', '243BK - Noir', 57.9),
(6, 'OKI ', 'Toner 5k - Noir', 85.9),
(7, 'Brother ', 'DR-2400 - Noir', 23.9),
(8, 'Epson ', '106 - Noir', 12.95),
(9, 'Epson ', '664 Ecotank Black ink bottle', 10.9),
(10, 'EPSON ', 'C13T664140 - Noir', 14.95),
(11, 'HP', ' 205A - Noir', 47.15),
(12, 'HP ', '304 - Noir', 15),
(13, 'HP ', '304 - Noir', 15),
(14, 'Canon ', 'PGI 570XL PGBK - Noir', 22.95),
(15, 'Brother TN', '2410 - Noir', 56.99),
(16, 'Canon ', 'PG-545 8ml - Noir', 24.8),
(17, 'HP ', '303 - Noir', 21.95),
(18, 'Brother TN', '423BK', 101),
(19, 'Brother', ' LC-3219XLBK', 39),
(20, 'Epson ', 'Singlepack Black 35 DURABrite', 33.9),
(21, 'Samsung ', 'MLT-D111S - Noir', 50.9),
(22, 'HP ', '711 - Noir', 34.3),
(23, 'Brother TN', '243BK - Noir ', 54.4),
(24, 'HP ', '62 - Noir', 14.95),
(25, 'HP ', '962 - Noir', 32.99),
(26, 'Canon ', 'CLI-581BK - Noir', 12.5),
(27, 'HP', '410A', 73.7),
(28, 'Brother TN', '2410 - Noir', 56.99),
(29, 'Brother TN', '243BK - Noir', 69),
(30, 'Brother TN', '421BK - Noir', 110);

-- --------------------------------------------------------

--
-- Structure de la table `t_price`
--

CREATE TABLE `t_price` (
  `id_EvolutionPrice` int(11) NOT NULL,
  `evYears` int(11) NOT NULL,
  `evPrice` float NOT NULL,
  `id_printer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_price`
--

INSERT INTO `t_price` (`id_EvolutionPrice`, `evYears`, `evPrice`, `id_printer`) VALUES
(1, 2019, 179.9, 1),
(2, 2018, 49.9, 2),
(3, 2019, 49.9, 2),
(4, 2019, 37.4, 3),
(5, 2019, 79.9, 4),
(6, 2019, 212, 5),
(7, 2019, 1049, 6),
(8, 2019, 162.19, 7),
(9, 2017, 650, 8),
(10, 2018, 639, 8),
(11, 2019, 578.9, 8),
(12, 2017, 239, 9),
(13, 2018, 215, 9),
(14, 2019, 175, 9),
(15, 2019, 113, 10),
(16, 2017, 250, 11),
(17, 2018, 248, 11),
(18, 2019, 209.9, 11),
(19, 2018, 64.9, 12),
(20, 2019, 56.9, 12),
(21, 2019, 37.4, 13),
(22, 2019, 89.95, 14),
(23, 2018, 152.95, 15),
(24, 2019, 134, 15),
(25, 2019, 40.5, 16),
(26, 2019, 110.4, 17),
(27, 2017, 717.9, 18),
(28, 2018, 678, 18),
(29, 2019, 511, 18),
(30, 2017, 216.9, 19),
(31, 2018, 199.8, 19),
(32, 2019, 150, 19),
(33, 2017, 284, 20),
(34, 2018, 255, 20),
(35, 2019, 210.9, 20),
(36, 2019, 203, 21),
(37, 2019, 763.2, 22),
(38, 2019, 200, 23),
(39, 2018, 63.9, 24),
(40, 2019, 30, 24),
(41, 2019, 186, 25),
(42, 2019, 79, 26),
(43, 2018, 70.9, 27),
(44, 2019, 96.42, 27),
(45, 2018, 103, 28),
(46, 2019, 63.2, 28),
(47, 2018, 437.95, 29),
(48, 2019, 378, 29),
(49, 2017, 456.45, 30),
(50, 2018, 438, 30),
(51, 2019, 377.99, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_printer`
--

CREATE TABLE `t_printer` (
  `id_printer` int(11) NOT NULL,
  `priModele` char(50) NOT NULL,
  `priMark` char(50) NOT NULL,
  `priWidth` int(11) NOT NULL,
  `priHeight` int(11) NOT NULL,
  `priLenght` int(11) NOT NULL,
  `priType` varchar(50) NOT NULL,
  `priWeight` int(11) NOT NULL,
  `priPrintSpeed` int(11) NOT NULL,
  `priPaper` varchar(4) NOT NULL,
  `priResolution` varchar(20) NOT NULL,
  `priReleaseDate` varchar(20) NOT NULL,
  `priLink` varchar(200) NOT NULL,
  `id_Consomable` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_printer`
--

INSERT INTO `t_printer` (`id_printer`, `priModele`, `priMark`, `priWidth`, `priHeight`, `priLenght`, `priType`, `priWeight`, `priPrintSpeed`, `priPaper`, `priResolution`, `priReleaseDate`, `priLink`, `id_Consomable`) VALUES
(1, 'HL-L3210CW', 'Brother', 461, 410, 252, 'Couleur', 18, 18, 'A4', '2400 x 600', '2018', 'https://www.brack.ch/fr/brother-imprimante-hl-l3210cw-798350?query=imprimantes&page=3', 1),
(2, 'Officejet 3833', 'HP', 450, 224, 364, 'Couleur', 6, 20, 'DL,B', '4800 x 1200', '2019', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-officejet-3833-couleur-wifi--p0001407477', 2),
(3, 'Deskjet 2620 white', 'HP', 304, 425, 149, 'Couleur', 3, 5, 'A4', '4800 x 1200', '2017', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/hp/deskjet-2620-white-8430302.html', 3),
(4, 'DCP-J572DW', 'Brother', 228, 457, 477, 'Couleur', 7, 6, 'A4', '6000 x 1200', '2019', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/brother/dcp-j572dw-8371184.html', 4),
(5, 'HL-L3270CDW', 'Brother', 439, 252, 461, 'Couleur', 19, 24, 'A4', '2400 x 600', '2018', 'https://www.steg-electronics.ch/fr/article/Brother-HL-L3270CDW-25246487.aspx', 5),
(6, 'C844dnw', 'Oki Electric Industry', 449, 360, 552, 'Couleur', 40, 36, 'A3', '1200 x 1200', '2019', 'https://www.steg-electronics.ch/fr/article/OKI-C844dnw-25906454.aspx', 6),
(7, 'HL-L2375DW', 'Brother', 356, 183, 360, 'Mono', 7, 34, 'A4', '1200 x 1200', '2018', 'https://www.steg-electronics.ch/fr/article/Brother-HL-L2375DW-24995873.aspx', 7),
(8, 'EcoTank ET-7750', 'Epson', 415, 526, 168, 'Couleur', 11, 26, 'A3+', '5760 x 1440', '2018', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/epson/ecotank-et-7750-8336892.html', 8),
(9, 'ET-2600', 'Epson', 445, 169, 304, 'Couleur', 5, 15, 'A4', '5760 x 1440', '2017', 'https://www.melectronics.ch/fr/p/797277300000/epson-ecotank-et-2600?gclid=CjwKCAjwtuLrBRAlEiwAPVcZBnEx0J65rpG4VU5ulIw6uEmzEVuCytkmW72BtStV7vvS10SaMH0RghoClH4QAvD_BwE&gclsrc=aw.ds', 9),
(10, 'LaserJet Pro M15', 'HP', 346, 159, 189, 'Couleur', 4, 18, 'A4', '600 x 600', '2019', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/hp-laserjet-pro-m15w--p0001500392?gclid=Cj0KCQjw6eTtBRDdARIsANZWjYbZAm8ndUw9ZT4iohX1nsCG-v8yYCSSkG-bY', 10),
(11, 'Laserjet Pro Color M181fw', 'HP', 380, 420, 341, 'Couleur', 16, 16, 'A4', '600 x 600', '2018', 'https://www.fust.ch/fr/p/ordinateur-tablette-mobile/imprimantes-et-scanners/imprimantes/hp/laserjet-pro-color-m181fw-8325499.html', 11),
(12, 'ENVY 5030', 'HP', 128, 445, 564, 'Couleur', 5, 20, 'A4,A', '4800 x 1200', '2018', 'https://www.mediamarkt.ch/fr/product/_hp-envy-5030-1810416.html?ga_query=HP+ENVY+5030', 12),
(13, 'Deskjet 2620 Blanc', 'HP', 304, 425, 149, 'Couleur', 3, 5, 'A4', '4800 x 1200', '2018', 'https://www.conforama.ch/fr/multimedia/informatique/imprimante/imprimante-hp-deskjet-2620-1n01b0-blanc/p/730826', 13),
(14, 'Pixma TS5055', 'Canon', 126, 372, 315, 'Couleur', 6, 12, 'A4,A', '4800x 1200', '2018', 'https://www.mediamarkt.ch/fr/product/_canon-pixma-ts5055-1710577.html', 14),
(15, 'DCP-L2530DW', 'Brother', 410, 272, 398, 'Mono', 10, 30, 'A4', '2400 x 600', '2018', 'https://www.melectronics.ch/fr/p/797280500000/brother-dcp-l2530dw?gclid=CjwKCAjwtuLrBRAlEiwAPVcZBh4cZZLCeerTbTUOTXvTZK0aQZFHGBz-M8VSJjVCWciCzI-JRybyYBoC2xQQAvD_BwE&gclsrc=aw.ds', 15),
(16, 'Pixma TR4550', 'Canon', 435, 180, 295, 'Couleur', 6, 8, 'A4', '4800 x 1200', '2018', 'https://www.melectronics.ch/fr/p/785300138320/canon-pixma-tr4550', 16),
(17, 'Tango X', 'HP', 389, 260, 615, 'Couleur', 3, 11, 'DL,B', '4800 x 1200', '2018', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-tango-x-couleur-wlan--p0001560608', 17),
(18, 'MFC-L8900CDW', 'Brother', 526, 549, 495, 'Couleur', 29, 32, 'A4,A', '2400 x 600', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l8900cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-6178338', 18),
(19, 'MFC-J5730DW', 'Brother', 398, 374, 530, 'Couleur', 21, 35, 'A3,A', '4800 x 1200', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-j5730dw-wifi-encre-couleur-impression-recto-verso-imprimantes-6068739', 19),
(20, 'WorkForcePro WF-4740DTWF', 'Epson', 425, 330, 388, 'Couleur', 12, 24, 'A4', '4800 x 1200', '2017', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/imprimante-multifonction-epson-workforcepro-wf-4740dtwf--p0001317537', 20),
(21, 'Color Laser MFP 178nw', 'HP', 406, 288, 363, 'Couleur', 13, 4, 'A4', '600 x 600', '2019', 'https://www.brack.ch/fr/hp-multifunktionsdrucker-color-laser-mfp-178nw-956819?query=drucker&forceLanguageChange', 21),
(22, 'T125 24 DesignJet', 'HP', 987, 285, 255, 'Couleur', 26, 45, 'A1,A', '1200 x 1200', '2019', 'https://www.digitec.ch/fr/s1/product/hp-t125-24-designjet-wifi-encre-couleur-imprimantes-11301355', 22),
(23, 'DCP-L3550CDW', 'Brother', 410, 414, 475, 'Couleur', 23, 18, 'A4,A', '2400 x 1200', '2018', 'https://www.digitec.ch/fr/s1/product/brother-dcp-l3550cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-9418839', 23),
(24, 'Deskjet 3733', 'HP', 403, 264, 451, 'Couleur', 2, 8, 'A4', '4800 x 1200 ', '2018', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-deskjet-3733-rouge--p0001398996?gclid=Cj0KCQjw6eTtBRDdARIsANZWjYYuSm301oUl-6cUxrl', 24),
(25, 'OfficeJet Pro 9015', 'HP', 439, 278, 342, 'Couleur', 9, 18, 'A4', '4800 x 1200', '2019', 'https://www.brack.ch/fr/hp-multifunktionsdrucker-officejet-pro-9015-all-in-one-951542?query=imprimante+all+in+one&redirected=1', 25),
(26, 'PIXMA TS6250', 'Canon', 372, 139, 315, 'Couleur', 6, 15, 'A4', '4800 x 1200', '2018', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/imprimante-multifonctions-canon-pixma-ts6250--p0001559967', 26),
(27, 'LaserJet Pro M102a', 'HP', 365, 191, 247, 'Mono', 5, 22, 'A4', '600 x 600 ', '2018', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/hp-laserjet-pro-m102a--p0001220741?gclid=Cj0KCQjw6eTtBRDdARIsANZWjYaj0TmBTJOa_Rfk4YMji8cq34bMRqVJI0Mc', 27),
(28, 'HL-L2350DW', 'Brother', 356, 183, 360, 'Mono', 7, 30, 'A5,A', '600 x 600', '2017', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/brother-hl-l2350dw-laser-led-noir-et-blanc--p0001430682', 28),
(29, 'MFC-L3770CDW', 'Brother', 509, 410, 414, 'Couleur', 25, 24, 'A4,A', '600 x 600', '2018', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l3770cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-9421947', 29),
(30, 'MFC-L8690CDW', 'Brother', 526, 539, 435, 'Couleur', 28, 28, 'A4', '1200 x 600', '2017', 'https://www.digitec.ch/fr/s1/product/brother-mfc-l8690cdw-wifi-laserled-couleur-impression-recto-verso-imprimantes-6178083', 30);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_client`
--
ALTER TABLE `t_client`
  ADD PRIMARY KEY (`id_Client`),
  ADD UNIQUE KEY `ID_t_Client_IND` (`id_Client`);

--
-- Index pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  ADD PRIMARY KEY (`id_Consomable`),
  ADD UNIQUE KEY `ID_t_consomable_IND` (`id_Consomable`);

--
-- Index pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD PRIMARY KEY (`id_EvolutionPrice`),
  ADD UNIQUE KEY `ID_t_Price_IND` (`id_EvolutionPrice`),
  ADD KEY `FKAvoir_IND` (`id_printer`);

--
-- Index pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD PRIMARY KEY (`id_printer`),
  ADD UNIQUE KEY `ID_t_Printer_IND` (`id_printer`) USING BTREE,
  ADD KEY `id_Consomable` (`id_Consomable`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_client`
--
ALTER TABLE `t_client`
  MODIFY `id_Client` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_consomable`
--
ALTER TABLE `t_consomable`
  MODIFY `id_Consomable` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `t_price`
--
ALTER TABLE `t_price`
  MODIFY `id_EvolutionPrice` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT pour la table `t_printer`
--
ALTER TABLE `t_printer`
  MODIFY `id_printer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_price`
--
ALTER TABLE `t_price`
  ADD CONSTRAINT `FKAvoir_FK` FOREIGN KEY (`id_printer`) REFERENCES `t_printer` (`id_printer`);

--
-- Contraintes pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD CONSTRAINT `t_printer_ibfk_1` FOREIGN KEY (`id_Consomable`) REFERENCES `t_consomable` (`id_Consomable`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
